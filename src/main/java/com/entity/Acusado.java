package com.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Cesar Balderas
 *
 */
@Entity
@Table(name = "ACUSADO") // <--- nombre de la vista en la base de datos

public class Acusado implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "APELLIDO_MATERNO", nullable = false)
	private String apellidoMaterno;
	
	@Column(name = "APELLIDO_PATERNO", nullable = false)
	private String apellidoPaterno;
	
	@Column(name = "NOMBRE", nullable = false)
	private String nombre;
	
	@Column(name = "EDAD", nullable = false)
	private Integer edad;
	
	@ManyToOne(optional =true)
    @JoinColumn(name="ID_DIALECTO")
	private Dialecto dialecto;
	
	@Column(name = "estatus", nullable = false)
	private boolean estatus;
	
	/**@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "ACUSADO_DELITO",
			joinColumns=@JoinColumn(name="acusado_id"),
			inverseJoinColumns=@JoinColumn(name="delito_id"))
	private List<Delito> delitos;
	**/
	
	@ManyToOne(optional =false)
    @JoinColumn(name="ID_DELITO")
	private Delito delito;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public Dialecto getDialecto() {
		return dialecto;
	}

	public void setDialecto(Dialecto dialecto) {
		this.dialecto = dialecto;
	}

	public boolean isEstatus() {
		return estatus;
	}

	public void setEstatus(boolean estatus) {
		this.estatus = estatus;
	}

	public Delito getDelito() {
		return delito;
	}

	public void setDelito(Delito delito) {
		this.delito = delito;
	}

}