package com.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.Transient;

/**
 * @author Cesar Balderas
 *
 */
@Entity
@Table(name = "MUNICIPIO") // <--- nombre de la vista en la base de datos

public class Municipio implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "NOMBRE", nullable = false)
	private String nombre;
	
	@Column(name = "DESCRIPCION", nullable = false)
	private String descripcion;
	
	@Column(name = "estatus", nullable = false)
	private boolean estatus;
	

	
	
	@Transient
	private String ramoV;
	
	public Municipio() {
	}

	public Municipio(Integer id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}
	
	public String getRamoV() {
		return ramoV;
	}

	public void setRamoV(String ramoV) {
		this.ramoV = ramoV;
	}

	public boolean isEstatus() {
		return estatus;
	}

	public void setEstatus(boolean estatus) {
		this.estatus = estatus;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	
}