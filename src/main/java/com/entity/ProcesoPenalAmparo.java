package com.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Cesar Balderas
 *
 */
@Entity
@Table(name = "PROCESO_PENAL_AMPARO") 

public class ProcesoPenalAmparo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@ManyToOne(optional =false)
    @JoinColumn(name="ID_PROCESO_PENAL")
	private ProcesoPenal procesoPenal;
	
	@ManyToOne(optional =false)
    @JoinColumn(name="ID_AUTORIDAD_FEDERAL")
	private Autoridad autoridadFederal;
	
	
	
	@Column(name = "FECHA_AMPARA", nullable = true)
	private Date fechaAmpara;
	
	@Column(name = "FECHA_PROTEGE", nullable = true)
	private Date fechaProtege;
	
	
	@Column(name = "FECHA_NIEGA", nullable = true)
	private Date fechaNiega;


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public ProcesoPenal getProcesoPenal() {
		return procesoPenal;
	}


	public void setProcesoPenal(ProcesoPenal procesoPenal) {
		this.procesoPenal = procesoPenal;
	}


	public Autoridad getAutoridadFederal() {
		return autoridadFederal;
	}


	public void setAutoridadFederal(Autoridad autoridadFederal) {
		this.autoridadFederal = autoridadFederal;
	}


	public Date getFechaAmpara() {
		return fechaAmpara;
	}


	public void setFechaAmpara(Date fechaAmpara) {
		this.fechaAmpara = fechaAmpara;
	}


	public Date getFechaProtege() {
		return fechaProtege;
	}


	public void setFechaProtege(Date fechaProtege) {
		this.fechaProtege = fechaProtege;
	}


	public Date getFechaNiega() {
		return fechaNiega;
	}


	public void setFechaNiega(Date fechaNiega) {
		this.fechaNiega = fechaNiega;
	}

	
	
	
	
}