package com.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Cesar Balderas
 *
 */
@Entity
@Table(name = "PENAL_AMPARO") // <--- nombre de la vista en la base de datos

public class PenalAmparo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	

	@Column(name = "NUMERO_AMPARO", nullable = false)
	private Long numeroAmparo;
	
	@Column(name = "FECHA_NOT_DEMANDA", nullable = false)
	private Date fechaNotDemanda;
	
	@Column(name = "FECHA_AMPARA", nullable = true)
	private Date fechaAmpara;
	
	@Column(name = "FECHA_PROTEJE", nullable = true)
	private Date fechaProteje;

	@Column(name = "FECHA_NIEGA", nullable = true)
	private Date fechaNiega;

	@Column(name = "FECHA_SOBREE", nullable = true)
	private Date fechaSobree;

	@Column(name = "ID_AUTORIDAD_FEDERAL", nullable = false)
	private Integer idAutoridadFederal;
	
	
	@Column(name = "estatus", nullable = false)
	private boolean estatus;

	
    @ManyToOne()
    @JoinColumn(name = "PROCESO_PENAL_ID")
	private ProcesoPenal ProcesoPenal;
	

	public boolean isEstatus() {
		return estatus;
	}

	public void setEstatus(boolean estatus) {
		this.estatus = estatus;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public Long getNumeroAmparo() {
		return numeroAmparo;
	}

	public void setNumeroAmparo(Long numeroAmparo) {
		this.numeroAmparo = numeroAmparo;
	}

	public Date getFechaNotDemanda() {
		return fechaNotDemanda;
	}

	public void setFechaNotDemanda(Date fechaNotDemanda) {
		this.fechaNotDemanda = fechaNotDemanda;
	}

	
	public Date getFechaAmpara() {
		return fechaAmpara;
	}

	public void setFechaAmpara(Date fechaAmpara) {
		this.fechaAmpara = fechaAmpara;
	}

	public Date getFechaProteje() {
		return fechaProteje;
	}

	public void setFechaProteje(Date fechaProteje) {
		this.fechaProteje = fechaProteje;
	}

	public Date getFechaNiega() {
		return fechaNiega;
	}

	public void setFechaNiega(Date fechaNiega) {
		this.fechaNiega = fechaNiega;
	}

	public Date getFechaSobree() {
		return fechaSobree;
	}

	public void setFechaSobree(Date fechaSobree) {
		this.fechaSobree = fechaSobree;
	}

		public Integer getIdAutoridadFederal() {
		return idAutoridadFederal;
	}

	public void setIdAutoridadFederal(Integer idAutoridadFederal) {
		this.idAutoridadFederal = idAutoridadFederal;
	}

	public ProcesoPenal getProcesoPenal() {
		return ProcesoPenal;
	}

	public void setProcesoPenal(ProcesoPenal procesoPenal) {
		ProcesoPenal = procesoPenal;
	}

	
	
	
}