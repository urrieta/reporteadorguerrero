package com.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Cesar Balderas
 *
 */
@Entity
@Table(name = "PROCESO_PENAL_FORMAL_PRISION") 

public class ProcesoPenalFormalPrision implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	
	@Column(name = "FECHA_LIBERTAD_FEPP", nullable = true)
	private Date fechaLibertadFepp;
	
	@Column(name = "FECHA_FORMAL_PRISION", nullable = true)
	private Date fechaFormalPrision;
	
	@Column(name = "FECHA_SUJESION", nullable = true)
	private Date fechaSujesion;
	
	@Column(name = "FECHA_APELACION", nullable = true)
	private Date fechaApelacion;
	
	@Column(name = "FECHA_JUCIO_ORDINARIO", nullable = true)
	private Date fechaJuicioOrdinario;
	
	@Column(name = "FECHA_JUCIO_SUMARIO", nullable = true)
	private Date fechaJuicioSumario;
	
	@Column(name = "FECHA_CIERRE_INSTRUCCIONES", nullable = true)
	private Date fechaCierreInstrucciones;
	
	@Column(name = "FECHA_AUDIENCIA_VISTA", nullable = true)
	private Date fechaAudienciaVista;
	
	@Column(name = "NUMERO_AUDIENCIA", nullable = true)
	private Date fechaAudiencia;
	
	@Column(name = "NUMERO_ACUERDO", nullable = true)
	private Date fechaAcuerdo;
	
	
	@ManyToOne(optional =false)
    @JoinColumn(name="ID_TIPO_SENTENCIA")
	private TipoSentencia tipoSentencia;
	
	
	
	@ManyToOne(optional =false)
    @JoinColumn(name="ID_PROCESO_PENAL")
	private ProcesoPenal procesoPenal;
	
	
	public ProcesoPenal getProcesoPenal() {
		return procesoPenal;
	}


	public void setProcesoPenal(ProcesoPenal procesoPenal) {
		this.procesoPenal = procesoPenal;
	}


	public Date getFechaLibertadFepp() {
		return fechaLibertadFepp;
	}


	public void setFechaLibertadFepp(Date fechaLibertadFepp) {
		this.fechaLibertadFepp = fechaLibertadFepp;
	}


	public Date getFechaFormalPrision() {
		return fechaFormalPrision;
	}


	public void setFechaFormalPrision(Date fechaFormalPrision) {
		this.fechaFormalPrision = fechaFormalPrision;
	}


	public Date getFechaSujesion() {
		return fechaSujesion;
	}


	public void setFechaSujesion(Date fechaSujesion) {
		this.fechaSujesion = fechaSujesion;
	}


	public Date getFechaApelacion() {
		return fechaApelacion;
	}


	public void setFechaApelacion(Date fechaApelacion) {
		this.fechaApelacion = fechaApelacion;
	}


	public Date getFechaJuicioOrdinario() {
		return fechaJuicioOrdinario;
	}


	public void setFechaJuicioOrdinario(Date fechaJuicioOrdinario) {
		this.fechaJuicioOrdinario = fechaJuicioOrdinario;
	}


	public Date getFechaJuicioSumario() {
		return fechaJuicioSumario;
	}


	public void setFechaJuicioSumario(Date fechaJuicioSumario) {
		this.fechaJuicioSumario = fechaJuicioSumario;
	}


	public Date getFechaCierreInstrucciones() {
		return fechaCierreInstrucciones;
	}


	public void setFechaCierreInstrucciones(Date fechaCierreInstrucciones) {
		this.fechaCierreInstrucciones = fechaCierreInstrucciones;
	}


	public Date getFechaAudienciaVista() {
		return fechaAudienciaVista;
	}


	public void setFechaAudienciaVista(Date fechaAudienciaVista) {
		this.fechaAudienciaVista = fechaAudienciaVista;
	}


	public Date getFechaAudiencia() {
		return fechaAudiencia;
	}


	public void setFechaAudiencia(Date fechaAudiencia) {
		this.fechaAudiencia = fechaAudiencia;
	}


	public Date getFechaAcuerdo() {
		return fechaAcuerdo;
	}


	public void setFechaAcuerdo(Date fechaAcuerdo) {
		this.fechaAcuerdo = fechaAcuerdo;
	}


	public TipoSentencia getTipoSentencia() {
		return tipoSentencia;
	}


	public void setTipoSentencia(TipoSentencia tipoSentencia) {
		this.tipoSentencia = tipoSentencia;
	}


	


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	
	
	
	
}