package com.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.annotation.Transient;

/**
 * @author Cesar Balderas
 *
 */
@Entity
@Table(name = "JUZGADO") // <--- nombre de la vista en la base de datos

public class Juzgado implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "NOMBRE", nullable = false)
	private String nombre;
	
	@Column(name = "DESCRIPCION", nullable = false)
	private String descripcion;
	
	@Column(name = "estatus", nullable = false)
	private boolean estatus;

	@ManyToOne(optional =false)
    @JoinColumn(name="ID_RAMO")
	private Ramo ramo;
	
	@ManyToOne(optional =false)
    @JoinColumn(name="ID_MUNICIPIO")
	private Municipio municipio;
	
	
	@ManyToOne(optional =false)
    @JoinColumn(name="ID_DISTRITO")
	private Distrito distrito;
	
	
	@Transient
	private Integer idRamoV;
	@Transient
	private Integer idMunicipioV;
	@Transient
	private Integer idDistritoV;

	
	

	public Integer getIdRamoV() {
		return idRamoV;
	}

	public void setIdRamoV(Integer idRamoV) {
		this.idRamoV = idRamoV;
	}

	public Integer getIdMunicipioV() {
		return idMunicipioV;
	}

	public void setIdMunicipioV(Integer idMunicipioV) {
		this.idMunicipioV = idMunicipioV;
	}

	public Integer getIdDistritoV() {
		return idDistritoV;
	}

	public void setIdDistritoV(Integer idDistritoV) {
		this.idDistritoV = idDistritoV;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public boolean isEstatus() {
		return estatus;
	}

	public void setEstatus(boolean estatus) {
		this.estatus = estatus;
	}

	

	public Distrito getDistrito() {
		return distrito;
	}

	public void setDistrito(Distrito distrito) {
		this.distrito = distrito;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	

	public Ramo getRamo() {
		return ramo;
	}

	public void setRamo(Ramo ramo) {
		this.ramo = ramo;
	}
	
}