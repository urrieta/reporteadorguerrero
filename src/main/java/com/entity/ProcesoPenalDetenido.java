package com.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Cesar Balderas
 *
 */
@Entity
@Table(name = "PROCESO_PENAL_DETENIDO") 

public class ProcesoPenalDetenido implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@ManyToOne(optional =false)
    @JoinColumn(name="ID_PROCESO_PENAL")
	private ProcesoPenal procesoPenal;

	
	@Column(name = "FECHA_DETENCION", nullable = true)
	private Date fechaDetencion;


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public ProcesoPenal getProcesoPenal() {
		return procesoPenal;
	}


	public void setProcesoPenal(ProcesoPenal procesoPenal) {
		this.procesoPenal = procesoPenal;
	}


	public Date getFechaDetencion() {
		return fechaDetencion;
	}


	public void setFechaDetencion(Date fechaDetencion) {
		this.fechaDetencion = fechaDetencion;
	}
	
	
	
	
	
}