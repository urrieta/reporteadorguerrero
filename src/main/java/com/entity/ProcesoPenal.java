package com.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Cesar Balderas
 *
 */
@Entity
@Table(name = "PROCESO_PENAL") 

public class ProcesoPenal implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	
	@Column(name = "ID_CAUSA_PENAL", nullable = false)
	private Integer idCausaPenal;
	
	@ManyToOne(optional =false)
    @JoinColumn(name="ID_PROCESADO")
	private Acusado procesado;
	
	@Column(name = "TIPO_DEFENSOR", nullable = false)
	private boolean tipoDefensor;
	
	@Column(name = "OBSERVACIONES", nullable = true)
	private String observaciones;
	
	@Column(name = "FECHA_DETENCION", nullable = true)
	private Date fechaDetencion;
	
	@Column(name = "FECHA_ULTIMA_ACTUACION", nullable = true)
	private Date fechaUltimaActuacion;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	

	public Integer getIdCausaPenal() {
		return idCausaPenal;
	}

	public void setIdCausaPenal(Integer idCausaPenal) {
		this.idCausaPenal = idCausaPenal;
	}

	public Acusado getProcesado() {
		return procesado;
	}

	public void setProcesado(Acusado procesado) {
		this.procesado = procesado;
	}

	public boolean isTipoDefensor() {
		return tipoDefensor;
	}

	public void setTipoDefensor(boolean tipoDefensor) {
		this.tipoDefensor = tipoDefensor;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}


	public Date getFechaDetencion() {
		return fechaDetencion;
	}

	public void setFechaDetencion(Date fechaDetencion) {
		this.fechaDetencion = fechaDetencion;
	}

	public Date getFechaUltimaActuacion() {
		return fechaUltimaActuacion;
	}

	public void setFechaUltimaActuacion(Date fechaUltimaActuacion) {
		this.fechaUltimaActuacion = fechaUltimaActuacion;
	}
	
	
	
}