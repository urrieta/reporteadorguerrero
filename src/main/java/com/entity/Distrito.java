package com.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * @author Cesar Balderas
 *
 */
@Entity
@Table(name = "DISTRITO") // <--- nombre de la vista en la base de datos
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","fieldHandler"})
public class Distrito implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "NOMBRE", nullable = false)
	private String nombre;
	
	@Column(name = "DESCRIPCION", nullable = false)
	private String descripcion;
	
	@Column(name = "estatus", nullable = false)
	private boolean estatus;
	
	
	@ManyToOne
    @JoinColumn(name="ID_MUNICIPIO")
	private Municipio municipio;
	
	
	@Transient
	private String municipioV;
	
	public Distrito() {
	}

	public Distrito(Integer id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}
	
	

	public boolean isEstatus() {
		return estatus;
	}

	public void setEstatus(boolean estatus) {
		this.estatus = estatus;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public String getMunicipioV() {
		return municipioV;
	}

	public void setMunicipioV(String municipioV) {
		this.municipioV = municipioV;
	}


	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	
}