package com.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "MATERIA_CIVIL")
public class MateriaCivil implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "EXPEDIENTE", nullable = false)
	private String expediente;
	
	@Column(name = "FOLIO", nullable = false)
	private String folio;
	
	@Column(name = "JUCIO", nullable = true)
	private String juicio;
	
	@Column(name = "ACTOR", nullable = true)
	private String actor;
	
	@Column(name = "DEMANDADO", nullable = true)
	private String demandado;
	
	@Column(name = "FECHA_INGRESO", nullable = false)
	private Date fechaIngreso;

	@Column(name = "estatus", nullable = false)
	private boolean estatus;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getExpediente() {
		return expediente;
	}

	public void setExpediente(String expediente) {
		this.expediente = expediente;
	}
	
	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getJuicio() {
		return juicio;
	}

	public void setJuicio(String juicio) {
		this.juicio = juicio;
	}

	public String getActor() {
		return actor;
	}

	public void setActor(String actor) {
		this.actor = actor;
	}

	public String getDemandado() {
		return demandado;
	}

	public void setDemandado(String demandado) {
		this.demandado = demandado;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public boolean isEstatus() {
		return estatus;
	}

	public void setEstatus(boolean estatus) {
		this.estatus = estatus;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
