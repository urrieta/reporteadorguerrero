package com.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Cesar Balderas
 *
 */
@Entity
@Table(name = "CAUSA_PENAL") 

public class CausaPenal implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "AVERIGUACION_PREVIA", nullable = false)
	private String averiguacionPrevia;
	
	@Column(name = "CAUSA_PENAL", nullable = false)
	private String causaPenal;
	
	@Column(name = "FECHA_INICIO", nullable = false)
	private Date fechaInicio;
	
	@Column(name = "CD", nullable = false)
	private boolean cd;
	
	@Column(name = "SC", nullable = false)
	private boolean sd;
	
	@ManyToOne(optional =false)
    @JoinColumn(name="ID_ACUSADO")
	private Acusado Acusado;
	
	@ManyToOne(optional =false)
    @JoinColumn(name="ID_AGRAVIADO")
	private Agraviado agraviado;
	
	@Column(name = "OBSERVACIONES", nullable = false)
	private String observaciones;
	
	@Column(name = "RADICADA", nullable = true)
	private Boolean radicada;

	@ManyToOne(optional =false)
    @JoinColumn(name="ID_JUZGADO")
	private Juzgado juzgado;
	
	
	public Juzgado getJuzgado() {
		return juzgado;
	}

	public void setJuzgado(Juzgado juzgado) {
		this.juzgado = juzgado;
	}

	public boolean isRadicada() {
		return radicada;
	}

	public void setRadicada(boolean radicada) {
		this.radicada = radicada;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCausaPenal() {
		return causaPenal;
	}

	public void setCausaPenal(String causaPenal) {
		this.causaPenal = causaPenal;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public boolean isCd() {
		return cd;
	}

	public void setCd(boolean cd) {
		this.cd = cd;
	}

	public boolean isSd() {
		return sd;
	}

	public void setSd(boolean sd) {
		this.sd = sd;
	}

	public Acusado getAcusado() {
		return Acusado;
	}

	public void setAcusado(Acusado acusado) {
		Acusado = acusado;
	}

	public Boolean getRadicada() {
		return radicada;
	}

	public void setRadicada(Boolean radicada) {
		this.radicada = radicada;
	}

	public Agraviado getAgraviado() {
		return agraviado;
	}

	public void setAgraviado(Agraviado agraviado) {
		this.agraviado = agraviado;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getAveriguacionPrevia() {
		return averiguacionPrevia;
	}

	public void setAveriguacionPrevia(String averiguacionPrevia) {
		this.averiguacionPrevia = averiguacionPrevia;
	}
	
	
	
	
}