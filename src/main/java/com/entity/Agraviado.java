package com.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Cesar Balderas
 *
 */
@Entity
@Table(name = "AGRAVIADO") // <--- nombre de la vista en la base de datos

public class Agraviado implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "NOMBRE", nullable = false)
	private String nombre;

	@Column(name = "APELLIDO_MATERNO", nullable = false)
	private String apellidoMaterno;
	
	@Column(name = "APELLIDO_PATERNO", nullable = false)
	private String apellidoPaterno;
	
	@Column(name = "EDAD", nullable = false)
	private Integer edad;
	
	@Column(name = "estatus", nullable = false)
	private boolean estatus;
	
	@ManyToOne(optional =true)
    @JoinColumn(name="ID_DIALECTO")
	private Dialecto dialecto;
	
	@Column(name = "ORDEN_APRENSION_LIBERADA", nullable = true)
	private Date ordenAprensionLiberada;
	@Column(name = "ORDEN_APRENSION_NEGADA", nullable = true)
	private Date ordenAprensionNegada;
	@Column(name = "ORDEN_APRENSION_EJECUTADA", nullable = true)
	private Date ordenAprensionEjecutada;
	@Column(name = "ORDEN_RE_APRENSION_LIBERADA", nullable = true)
	private Date ordenReAprensionLiberada;
	@Column(name = "ORDEN_RE_APRENSION_EJECUTADA", nullable = true)
	private Date ordenReAprensionEjecutada;

	public Date getOrdenAprensionLiberada() {
		return ordenAprensionLiberada;
	}

	public void setOrdenAprensionLiberada(Date ordenAprensionLiberada) {
		this.ordenAprensionLiberada = ordenAprensionLiberada;
	}

	public Date getOrdenAprensionNegada() {
		return ordenAprensionNegada;
	}

	public void setOrdenAprensionNegada(Date ordenAprensionNegada) {
		this.ordenAprensionNegada = ordenAprensionNegada;
	}

	public Date getOrdenAprensionEjecutada() {
		return ordenAprensionEjecutada;
	}

	public void setOrdenAprensionEjecutada(Date ordenAprensionEjecutada) {
		this.ordenAprensionEjecutada = ordenAprensionEjecutada;
	}

	public Date getOrdenReAprensionLiberada() {
		return ordenReAprensionLiberada;
	}

	public void setOrdenReAprensionLiberada(Date ordenReAprensionLiberada) {
		this.ordenReAprensionLiberada = ordenReAprensionLiberada;
	}

	public Date getOrdenReAprensionEjecutada() {
		return ordenReAprensionEjecutada;
	}

	public void setOrdenReAprensionEjecutada(Date ordenReAprensionEjecutada) {
		this.ordenReAprensionEjecutada = ordenReAprensionEjecutada;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean isEstatus() {
		return estatus;
	}

	public void setEstatus(boolean estatus) {
		this.estatus = estatus;
	}

	public Dialecto getDialecto() {
		return dialecto;
	}

	public void setDialecto(Dialecto dialecto) {
		this.dialecto = dialecto;
	}

	
}