package com.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



@Entity
@Table(name = "MATERIA_CIVIL_AMPARO")
public class MateriaCivilAmparo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "NUMERO_AMPARO", nullable = false)
	private Long numeroAmparo;
	
	@Column(name = "FECHA_DEMANDA_AMAPARO", nullable = false)
	private Date fechaIngreso;
	
	@Column(name = "QUEJOSO", nullable = false)
	private String quejoso;
	
	@Column(name = "ACTO_RECLAMADO", nullable = false)
	private String actoReclamado;
	
	@Column(name = "TERCER_INTERESADO", nullable = false)
	private String tercerinteesado;
	
	@Column(name = "ID_AUTORIDAD_FEDERAL", nullable = false)
	private Integer idAutoridadFederal;
	
	@Column(name = "AMPARA_EFECTOS", nullable = false)
	private String amparaEfectos;
	
	@Column(name = "FECHA_PROTEJE", nullable = true)
	private Date fechaProteje;
	
	@Column(name = "FECHA_NIEGA", nullable = true)
	private Date fechaNiega;

	@Column(name = "FECHA_SOBREE", nullable = true)
	private Date fechaSobree;
	
	@Column(name = "estatus", nullable = false)
	private boolean estatus;
	
	@ManyToOne()
    @JoinColumn(name = "MATERIA_CIVIL_ID")
	private MateriaCivil materiaCivil;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getNumeroAmparo() {
		return numeroAmparo;
	}

	public void setNumeroAmparo(Long numeroAmparo) {
		this.numeroAmparo = numeroAmparo;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public String getQuejoso() {
		return quejoso;
	}

	public void setQuejoso(String quejoso) {
		this.quejoso = quejoso;
	}

	public String getActoReclamado() {
		return actoReclamado;
	}

	public void setActoReclamado(String actoReclamado) {
		this.actoReclamado = actoReclamado;
	}

	public String getTercerinteesado() {
		return tercerinteesado;
	}

	public void setTercerinteesado(String tercerinteesado) {
		this.tercerinteesado = tercerinteesado;
	}

	public Integer getIdAutoridadFederal() {
		return idAutoridadFederal;
	}

	public void setIdAutoridadFederal(Integer idAutoridadFederal) {
		this.idAutoridadFederal = idAutoridadFederal;
	}

	public String getAmparaEfectos() {
		return amparaEfectos;
	}

	public void setAmparaEfectos(String amparaEfectos) {
		this.amparaEfectos = amparaEfectos;
	}

	public Date getFechaProteje() {
		return fechaProteje;
	}

	public void setFechaProteje(Date fechaProteje) {
		this.fechaProteje = fechaProteje;
	}

	public Date getFechaNiega() {
		return fechaNiega;
	}

	public void setFechaNiega(Date fechaNiega) {
		this.fechaNiega = fechaNiega;
	}

	public Date getFechaSobree() {
		return fechaSobree;
	}

	public void setFechaSobree(Date fechaSobree) {
		this.fechaSobree = fechaSobree;
	}

	public boolean isEstatus() {
		return estatus;
	}

	public void setEstatus(boolean estatus) {
		this.estatus = estatus;
	}

	public MateriaCivil getMateriaCivil() {
		return materiaCivil;
	}

	public void setMateriaCivil(MateriaCivil materiaCivil) {
		this.materiaCivil = materiaCivil;
	}
	
	
	
}
