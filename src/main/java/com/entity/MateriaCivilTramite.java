package com.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "MATERIA_CIVIL_TRAMITE")
public class MateriaCivilTramite implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "ID_DEMANDA_ACUSE", nullable = false)
	private Integer idDemandaAcuse;
	
	@Column(name = "OFRECIMIENTO_PRUEBAS", nullable = false)
	private boolean ofrecimientoPruebas;

	@Column(name = "DESAHOGO_PRUEBAS", nullable = true)
	private Date desahogoPruebas;
	
	@Column(name = "ALEGATOS", nullable = false)
	private boolean alegatos;
	
	@Column(name = "CITACION_SENTENCIA", nullable = true)
	private Date citacionSentencia;
	
	@Column(name = "DESISTIMIENTO", nullable = false)
	private String desistimiento;
	
	@Column(name = "CADUCIDAD", nullable = true)
	private Date caducidad;
	
	@Column(name = "FECHA_RESOLUCION", nullable = true)
	private Date fechaResolucion;
	
	@Column(name = "DEFINITIVA", nullable = false)
	private boolean definitiva;
	
	@Column(name = "INTERLOCUTORA", nullable = false)
	private boolean interlocutora;
	
	@Column(name = "AUTO_TRAMITE", nullable = false)
	private Long autoTramite;
	
	@Column(name = "AUTO_FECHA_DEFINITIVO", nullable = true)
	private Date autoFechaDefinitivo;
	
	@Column(name = "APELACION_SENTENCIA", nullable = false)
	private boolean apelacionSentencia;
	
	@Column(name = "APELACION_AUTO_FECHA", nullable = true)
	private Date apelacionAutoFecha;
	
	@Column(name = "NOMERO_AUDIENCIA", nullable = false)
	private Long numeroAudiencia;
	
	@Column(name = "estatus", nullable = false)
	private boolean estatus;
	
	@ManyToOne()
    @JoinColumn(name = "MATERIA_CIVIL_ID")
	private MateriaCivil materiaCivil;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdDemandaAcuse() {
		return idDemandaAcuse;
	}

	public void setIdDemandaAcuse(Integer idDemandaAcuse) {
		this.idDemandaAcuse = idDemandaAcuse;
	}

	public boolean isOfrecimientoPruebas() {
		return ofrecimientoPruebas;
	}

	public void setOfrecimientoPruebas(boolean ofrecimientoPruebas) {
		this.ofrecimientoPruebas = ofrecimientoPruebas;
	}

	public Date getDesahogoPruebas() {
		return desahogoPruebas;
	}

	public void setDesahogoPruebas(Date desahogoPruebas) {
		this.desahogoPruebas = desahogoPruebas;
	}

	public boolean isAlegatos() {
		return alegatos;
	}

	public void setAlegatos(boolean alegatos) {
		this.alegatos = alegatos;
	}

	public Date getCitacionSentencia() {
		return citacionSentencia;
	}

	public void setCitacionSentencia(Date citacionSentencia) {
		this.citacionSentencia = citacionSentencia;
	}

	public String getDesistimiento() {
		return desistimiento;
	}

	public void setDesistimiento(String desistimiento) {
		this.desistimiento = desistimiento;
	}

	public Date getCaducidad() {
		return caducidad;
	}

	public void setCaducidad(Date caducidad) {
		this.caducidad = caducidad;
	}

	public Date getFechaResolucion() {
		return fechaResolucion;
	}

	public void setFechaResolucion(Date fechaResolucion) {
		this.fechaResolucion = fechaResolucion;
	}

	public boolean isDefinitiva() {
		return definitiva;
	}

	public void setDefinitiva(boolean definitiva) {
		this.definitiva = definitiva;
	}

	public boolean isInterlocutora() {
		return interlocutora;
	}

	public void setInterlocutora(boolean interlocutora) {
		this.interlocutora = interlocutora;
	}

	public Long getAutoTramite() {
		return autoTramite;
	}

	public void setAutoTramite(Long autoTramite) {
		this.autoTramite = autoTramite;
	}

	public Date getAutoFechaDefinitivo() {
		return autoFechaDefinitivo;
	}

	public void setAutoFechaDefinitivo(Date autoFechaDefinitivo) {
		this.autoFechaDefinitivo = autoFechaDefinitivo;
	}

	public boolean isApelacionSentencia() {
		return apelacionSentencia;
	}

	public void setApelacionSentencia(boolean apelacionSentencia) {
		this.apelacionSentencia = apelacionSentencia;
	}

	public Date getApelacionAutoFecha() {
		return apelacionAutoFecha;
	}

	public void setApelacionAutoFecha(Date apelacionAutoFecha) {
		this.apelacionAutoFecha = apelacionAutoFecha;
	}

	public Long getNumeroAudiencia() {
		return numeroAudiencia;
	}

	public void setNumeroAudiencia(Long numeroAudiencia) {
		this.numeroAudiencia = numeroAudiencia;
	}

	public MateriaCivil getMateriaCivil() {
		return materiaCivil;
	}

	public void setMateriaCivil(MateriaCivil materiaCivil) {
		this.materiaCivil = materiaCivil;
	}

	public boolean isEstatus() {
		return estatus;
	}

	public void setEstatus(boolean estatus) {
		this.estatus = estatus;
	}

}
