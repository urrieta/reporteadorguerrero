package com.dto;

import java.io.Serializable;
import java.util.Date;

public class ProcesoPenalVO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private int idCausaPenal;
	private Date fechaDetencion;
	private Date fechaUltimaActuacion;
	private String observaciones;
	private Date fechaAmpara;
	private Date fechaProtege;
	private Date fechaNiega;
	private int tipoProceso;
	private String  observacionesBajoFianza;
	private Date fechaFormalPrision;
	private Date fechaSujesion;
	private Date fechaApelacion;
	private Date fechaJuicioOrdinario;
	private Date fechaJuicioSumario;
	private Date fechaCierreInstrucciones;
	private Date fechaAudienciaVista;
	private Date fechaAudiencia;
	private Date fechaAcuerdo;
	private Integer sentencia;

	
	public Integer getSentencia() {
		return sentencia;
	}
	public void setSentencia(Integer sentencia) {
		this.sentencia = sentencia;
	}
	public String getObservacionesBajoFianza() {
		return observacionesBajoFianza;
	}
	public void setObservacionesBajoFianza(String observacionesBajoFianza) {
		this.observacionesBajoFianza = observacionesBajoFianza;
	}
	public int getTipoProceso() {
		return tipoProceso;
	}
	public void setTipoProceso(int tipoProceso) {
		this.tipoProceso = tipoProceso;
	}
	public Date getFechaAmpara() {
		return fechaAmpara;
	}
	public void setFechaAmpara(Date fechaAmpara) {
		this.fechaAmpara = fechaAmpara;
	}
	public Date getFechaProtege() {
		return fechaProtege;
	}
	public void setFechaProtege(Date fechaProtege) {
		this.fechaProtege = fechaProtege;
	}
	public Date getFechaNiega() {
		return fechaNiega;
	}
	public void setFechaNiega(Date fechaNiega) {
		this.fechaNiega = fechaNiega;
	}
	public int getIdCausaPenal() {
		return idCausaPenal;
	}
	public void setIdCausaPenal(int idCausaPenal) {
		this.idCausaPenal = idCausaPenal;
	}
	public Date getFechaDetencion() {
		return fechaDetencion;
	}
	public void setFechaDetencion(Date fechaDetencion) {
		this.fechaDetencion = fechaDetencion;
	}
	public Date getFechaUltimaActuacion() {
		return fechaUltimaActuacion;
	}
	public void setFechaUltimaActuacion(Date fechaUltimaActuacion) {
		this.fechaUltimaActuacion = fechaUltimaActuacion;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public Date getFechaFormalPrision() {
		return fechaFormalPrision;
	}
	public void setFechaFormalPrision(Date fechaFormalPrision) {
		this.fechaFormalPrision = fechaFormalPrision;
	}
	public Date getFechaSujesion() {
		return fechaSujesion;
	}
	public void setFechaSujesion(Date fechaSujesion) {
		this.fechaSujesion = fechaSujesion;
	}
	public Date getFechaApelacion() {
		return fechaApelacion;
	}
	public void setFechaApelacion(Date fechaApelacion) {
		this.fechaApelacion = fechaApelacion;
	}
	public Date getFechaJuicioOrdinario() {
		return fechaJuicioOrdinario;
	}
	public void setFechaJuicioOrdinario(Date fechaJuicioOrdinario) {
		this.fechaJuicioOrdinario = fechaJuicioOrdinario;
	}
	public Date getFechaJuicioSumario() {
		return fechaJuicioSumario;
	}
	public void setFechaJuicioSumario(Date fechaJuicioSumario) {
		this.fechaJuicioSumario = fechaJuicioSumario;
	}
	public Date getFechaCierreInstrucciones() {
		return fechaCierreInstrucciones;
	}
	public void setFechaCierreInstrucciones(Date fechaCierreInstrucciones) {
		this.fechaCierreInstrucciones = fechaCierreInstrucciones;
	}
	public Date getFechaAudienciaVista() {
		return fechaAudienciaVista;
	}
	public void setFechaAudienciaVista(Date fechaAudienciaVista) {
		this.fechaAudienciaVista = fechaAudienciaVista;
	}
	public Date getFechaAudiencia() {
		return fechaAudiencia;
	}
	public void setFechaAudiencia(Date fechaAudiencia) {
		this.fechaAudiencia = fechaAudiencia;
	}
	public Date getFechaAcuerdo() {
		return fechaAcuerdo;
	}
	public void setFechaAcuerdo(Date fechaAcuerdo) {
		this.fechaAcuerdo = fechaAcuerdo;
	}

	
	
	
}
