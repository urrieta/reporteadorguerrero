package com.dto;

import java.io.Serializable;
import java.util.Date;

public class CausaPenalDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String nombreDistrito;
	private Date fechaInicio;
	private String acusadoNombre;
	private String acusadoApellidoPaterno;
	private String acusadoApellidoMaterno;
	private int edadAcusado;
	private String agraviadoNombre;
	private String agraviadoApellidoPaterno;
	private String agraviadoApellidoMaterno;
	private int edadAgraviado;
	private Date fechaAprensionLiberacion;
	private Date fechaAprensionNegacion;
	private Date fechaAprensionEjecucion;
	private boolean cd;
	private boolean sc;
	private String nombreLenguaAcusado;
	private String nombreLenguaAgraviado;
	private String nombreDelito;
	private String carpetaJudicial;
	private String cdC;
	private String scC;


	public String getCdC() {
		return cdC;
	}
	public void setCdC(String cdC) {
		this.cdC = cdC;
	}
	public String getScC() {
		return scC;
	}
	public void setScC(String scC) {
		this.scC = scC;
	}
	public String getCarpetaJudicial() {
		return carpetaJudicial;
	}
	public void setCarpetaJudicial(String carpetaJudicial) {
		this.carpetaJudicial = carpetaJudicial;
	}
	public String getNombreDistrito() {
		return nombreDistrito;
	}
	public void setNombreDistrito(String nombreDistrito) {
		this.nombreDistrito = nombreDistrito;
	}
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	
	public String getAcusadoNombre() {
		return acusadoNombre;
	}
	public void setAcusadoNombre(String acusadoNombre) {
		this.acusadoNombre = acusadoNombre;
	}
	public String getAcusadoApellidoPaterno() {
		return acusadoApellidoPaterno;
	}
	public void setAcusadoApellidoPaterno(String acusadoApellidoPaterno) {
		this.acusadoApellidoPaterno = acusadoApellidoPaterno;
	}
	public String getAcusadoApellidoMaterno() {
		return acusadoApellidoMaterno;
	}
	public void setAcusadoApellidoMaterno(String acusadoApellidoMaterno) {
		this.acusadoApellidoMaterno = acusadoApellidoMaterno;
	}
	
	
	
	public int getEdadAcusado() {
		return edadAcusado;
	}
	public void setEdadAcusado(int edadAcusado) {
		this.edadAcusado = edadAcusado;
	}
	public int getEdadAgraviado() {
		return edadAgraviado;
	}
	public void setEdadAgraviado(int edadAgraviado) {
		this.edadAgraviado = edadAgraviado;
	}
	public boolean isCd() {
		return cd;
	}
	public void setCd(boolean cd) {
		this.cd = cd;
	}
	public boolean isSc() {
		return sc;
	}
	public void setSc(boolean sc) {
		this.sc = sc;
	}
	public String getNombreLenguaAcusado() {
		return nombreLenguaAcusado;
	}
	public void setNombreLenguaAcusado(String nombreLenguaAcusado) {
		this.nombreLenguaAcusado = nombreLenguaAcusado;
	}
	public String getNombreLenguaAgraviado() {
		return nombreLenguaAgraviado;
	}
	public void setNombreLenguaAgraviado(String nombreLenguaAgraviado) {
		this.nombreLenguaAgraviado = nombreLenguaAgraviado;
	}
	public String getAgraviadoNombre() {
		return agraviadoNombre;
	}
	public void setAgraviadoNombre(String agraviadoNombre) {
		this.agraviadoNombre = agraviadoNombre;
	}
	public String getAgraviadoApellidoPaterno() {
		return agraviadoApellidoPaterno;
	}
	public void setAgraviadoApellidoPaterno(String agraviadoApellidoPaterno) {
		this.agraviadoApellidoPaterno = agraviadoApellidoPaterno;
	}
	public String getAgraviadoApellidoMaterno() {
		return agraviadoApellidoMaterno;
	}
	
	public String getNombreDelito() {
		return nombreDelito;
	}
	public void setNombreDelito(String nombreDelito) {
		this.nombreDelito = nombreDelito;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setAgraviadoApellidoMaterno(String agraviadoApellidoMaterno) {
		this.agraviadoApellidoMaterno = agraviadoApellidoMaterno;
	}
	public Date getFechaAprensionLiberacion() {
		return fechaAprensionLiberacion;
	}
	public void setFechaAprensionLiberacion(Date fechaAprensionLiberacion) {
		this.fechaAprensionLiberacion = fechaAprensionLiberacion;
	}
	public Date getFechaAprensionNegacion() {
		return fechaAprensionNegacion;
	}
	public void setFechaAprensionNegacion(Date fechaAprensionNegacion) {
		this.fechaAprensionNegacion = fechaAprensionNegacion;
	}
	public Date getFechaAprensionEjecucion() {
		return fechaAprensionEjecucion;
	}
	public void setFechaAprensionEjecucion(Date fechaAprensionEjecucion) {
		this.fechaAprensionEjecucion = fechaAprensionEjecucion;
	}

	
	
}
