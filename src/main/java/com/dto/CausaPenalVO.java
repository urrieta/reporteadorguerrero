package com.dto;

import java.io.Serializable;
import java.util.Date;

public class CausaPenalVO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private Integer id;
	private String averiguacionPrevia;
	private String causaPenal;
	private Long acusado;
	private Long agraviado;
	private Long delito;
	private String observaciones;
	private boolean radicada;
	private boolean cd;
	private boolean sd;
	private Date fechaInicio;
	
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public boolean isCd() {
		return cd;
	}
	public void setCd(boolean cd) {
		this.cd = cd;
	}
	public boolean isSd() {
		return sd;
	}
	public void setSd(boolean sd) {
		this.sd = sd;
	}
	public boolean isRadicada() {
		return radicada;
	}
	public void setRadicada(boolean radicada) {
		this.radicada = radicada;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAveriguacionPrevia() {
		return averiguacionPrevia;
	}
	public void setAveriguacionPrevia(String averiguacionPrevia) {
		this.averiguacionPrevia = averiguacionPrevia;
	}
	public String getCausaPenal() {
		return causaPenal;
	}
	public void setCausaPenal(String causaPenal) {
		this.causaPenal = causaPenal;
	}
	public Long getAcusado() {
		return acusado;
	}
	public void setAcusado(Long acusado) {
		this.acusado = acusado;
	}
	public Long getAgraviado() {
		return agraviado;
	}
	public void setAgraviado(Long agraviado) {
		this.agraviado = agraviado;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public Long getDelito() {
		return delito;
	}
	public void setDelito(Long delito) {
		this.delito = delito;
	}
	
	
}
