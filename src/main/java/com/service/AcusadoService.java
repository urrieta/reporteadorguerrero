package com.service;

import java.util.List;

import com.entity.Acusado;

public interface AcusadoService {
	public abstract List<Acusado> listAllAcusados();
	public abstract Acusado addAcusado(Acusado acusado);
	public abstract void removeAcusado(int id);
	public abstract Acusado updatAcusado(Acusado acusado);
	public abstract Acusado findById(Integer id);
	public abstract List<Acusado> findByEstatus(boolean estatus);
	public List<Acusado> findByIdPenalCausaPenal(Integer id);

}
