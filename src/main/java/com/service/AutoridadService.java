package com.service;

import java.util.List;

import com.entity.Autoridad;

public interface AutoridadService {
	public abstract List<Autoridad> listAllAutoridads();
	public abstract Autoridad addAutoridad(Autoridad autoridad);
	public abstract void removeAutoridad(int id);
	public abstract Autoridad updatAutoridad(Autoridad autoridad);
	public abstract Autoridad findById(Integer id);
	public abstract List<Autoridad> findByEstatus(boolean estatus);

}
