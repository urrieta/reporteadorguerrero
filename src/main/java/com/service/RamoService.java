package com.service;

import java.util.List;

import com.entity.Ramo;

public interface RamoService {
	public abstract List<Ramo> listAllRamos();
	public abstract Ramo addRamo(Ramo Ramo);
	public abstract void removeRamo(int id);
	public abstract Ramo updatRamo(Ramo Ramo);
	public abstract Ramo findById(Integer id);
	public abstract List<Ramo> findByEstatus(boolean estatus);

}
