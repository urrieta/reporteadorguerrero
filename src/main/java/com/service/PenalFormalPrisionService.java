package com.service;

import java.util.List;

import com.entity.ProcesoPenalFormalPrision;

public interface PenalFormalPrisionService {
	public abstract List<ProcesoPenalFormalPrision> listAllProcesoPenalFormalPrision();
	public abstract ProcesoPenalFormalPrision addProcesoPenalFormalPrision(ProcesoPenalFormalPrision entity);
	public abstract void removeProcesoPenalFormalPrision(int id);
	public abstract ProcesoPenalFormalPrision updatProcesoPenalFormalPrision(ProcesoPenalFormalPrision entity);
	public abstract ProcesoPenalFormalPrision findById(Integer id);
	public abstract ProcesoPenalFormalPrision findByProcesoPenalId(Integer id);

}
