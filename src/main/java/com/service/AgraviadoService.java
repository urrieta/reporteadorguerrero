package com.service;

import java.util.List;

import com.entity.Agraviado;

public interface AgraviadoService {
	public abstract List<Agraviado> listAllAgraviados();
	public abstract Agraviado addAgraviado(Agraviado agraviado);
	public abstract void removeAgraviado(int id);
	public abstract Agraviado updatAgraviado(Agraviado agraviado);
	public abstract Agraviado findById(Integer id);
	public abstract List<Agraviado> findByEstatus(boolean estatus);
	public List<Agraviado> findByIdPenalCausaPenal(Integer id);

}
