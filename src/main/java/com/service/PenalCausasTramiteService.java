package com.service;

import java.util.List;
import com.entity.PenalCausasTramite;

public interface PenalCausasTramiteService {
	public abstract List<PenalCausasTramite> listAllPenalCausasTramites();
	public abstract PenalCausasTramite addPenalCausasTramite(PenalCausasTramite entity);
	public abstract void removePenalCausasTramite(int id);
	public abstract PenalCausasTramite updatPenalCausasTramite(PenalCausasTramite entity);
	public abstract PenalCausasTramite findById(Integer id);
	public abstract List<PenalCausasTramite> findByEstatus(boolean estatus);

}
