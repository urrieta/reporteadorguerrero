package com.service;

import java.util.List;

import com.entity.Delito;

public interface DelitoService {
	public abstract List<Delito> listAllDelitos();
	public abstract Delito addDelito(Delito delito);
	public abstract void removeDelito(int id);
	public abstract Delito updatDelito(Delito delito);
	public abstract Delito findById(Integer id);
	public abstract List<Delito> findByEstatus(boolean estatus);

}
