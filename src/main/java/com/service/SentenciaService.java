package com.service;

import java.util.List;

import com.entity.TipoSentencia;

public interface SentenciaService {
	public abstract List<TipoSentencia> listAll();
	public abstract TipoSentencia add(TipoSentencia entity);
	public abstract void remove(int id);
	public abstract TipoSentencia updat(TipoSentencia entity);
	public abstract TipoSentencia findById(Integer id);
	public abstract List<TipoSentencia> findByEstatus(boolean estatus);

}
