package com.service;

import java.util.List;

import com.entity.MateriaCivilTramite;

public interface MateriaCivilTramiteService {
	public abstract List<MateriaCivilTramite> listAllMateriaCivilTramites();
	public abstract MateriaCivilTramite addMateriaCivilTramite(MateriaCivilTramite entity);
	public abstract void removeMateriaCivilTramite(int id);
	public abstract MateriaCivilTramite updateMateriaCivilTramite(MateriaCivilTramite entity);
	public abstract MateriaCivilTramite findById(Integer id);
	public abstract List<MateriaCivilTramite> findByEstatus(boolean estatus);
}
