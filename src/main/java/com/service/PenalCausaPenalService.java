package com.service;

import java.util.List;

import com.dto.ReporteAdminPenalUnoDTO;
import com.entity.CausaPenal;

public interface PenalCausaPenalService {
	public abstract List<CausaPenal> listAllPenalCausaPenals();
	public abstract CausaPenal addPenalCausaPenal(CausaPenal entity);
	public abstract void removePenalCausaPenal(int id);
	public abstract CausaPenal findById(Integer id);
	public abstract List<CausaPenal> findByEstatus(boolean estatus);
	public abstract List<ReporteAdminPenalUnoDTO>  reporteAdminPenalUno();
	public abstract CausaPenal updatePenal(CausaPenal entity);

}
