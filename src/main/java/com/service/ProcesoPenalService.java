package com.service;

import java.util.List;

import com.entity.ProcesoPenal;

public interface ProcesoPenalService {
	public abstract List<ProcesoPenal> listAllProcesoPenal();
	public abstract ProcesoPenal addProcesoPenal(ProcesoPenal entity);
	public abstract void removeProcesoPenal(int id);
	public abstract ProcesoPenal update(ProcesoPenal entity);
	public abstract ProcesoPenal findById(Integer id);
	public abstract List<ProcesoPenal> findByEstatus(boolean estatus);
	public abstract ProcesoPenal findByIdCausaPenal(Integer id);

}
