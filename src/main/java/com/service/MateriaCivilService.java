package com.service;

import java.util.List;

import com.entity.MateriaCivil;

public interface MateriaCivilService {
	public abstract List<MateriaCivil> listAllMateriaCivil();
	public abstract MateriaCivil findById(Integer id);
	public abstract MateriaCivil addMateriaCivil(MateriaCivil entity);
	public abstract void removeMateriaCivil(int id);
	public abstract MateriaCivil updateMateriaCivil(MateriaCivil entity);
	public abstract List<MateriaCivil> findByEstatus(boolean estatus);
}
