package com.service;

import java.util.List;

import com.entity.Municipio;

public interface MunicipioService {
	public abstract List<Municipio> listAllMunicipios();
	public abstract Municipio addMunicipio(Municipio municipio);
	public abstract void removeMunicipio(int id);
	public abstract Municipio updatMunicipio(Municipio municipio);
	public abstract Municipio findById(Integer id);
	public abstract List<Municipio> findByEstatus(boolean estatus);

}
