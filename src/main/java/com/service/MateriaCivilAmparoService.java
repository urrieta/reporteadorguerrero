package com.service;

import java.util.List;

import com.entity.MateriaCivilAmparo;

public interface MateriaCivilAmparoService {
	public abstract List<MateriaCivilAmparo> listAllMateriaCivilAmparos();
	public abstract MateriaCivilAmparo addMateriaCivilAmparo(MateriaCivilAmparo entity);
	public abstract void removeMateriaCivilAmparo(int id);
	public abstract MateriaCivilAmparo updateMateriaCivilAmparo(MateriaCivilAmparo entity);
	public abstract MateriaCivilAmparo findById(Integer id);
	public abstract List<MateriaCivilAmparo> findByEstatus(boolean estatus);
}
