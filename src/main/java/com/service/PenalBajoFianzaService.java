package com.service;

import java.util.List;

import com.entity.ProcesoPenalBajoFianza;

public interface PenalBajoFianzaService {
	public abstract List<ProcesoPenalBajoFianza> listAllProcesoPenalBajoFianza();
	public abstract ProcesoPenalBajoFianza addProcesoPenalBajoFianza(ProcesoPenalBajoFianza entity);
	public abstract void removeProcesoPenalBajoFianza(int id);
	public abstract ProcesoPenalBajoFianza updatProcesoPenalBajoFianza(ProcesoPenalBajoFianza entity);
	public abstract ProcesoPenalBajoFianza findById(Integer id);
	public abstract ProcesoPenalBajoFianza findByProcesoPenalId(Integer id);

}
