package com.service;

import java.util.List;

import com.entity.TipoCausaPenal;

public interface TipoCausaPenalService {
	public abstract List<TipoCausaPenal> listAll();
	public abstract TipoCausaPenal addTipoCausaPenal(TipoCausaPenal entidad);
	public abstract void removeAcusado(int id);
	public abstract TipoCausaPenal updatTipoCausaPenal(TipoCausaPenal entidad);
	public abstract TipoCausaPenal findById(Integer id);
	public abstract List<TipoCausaPenal> findByEstatus(boolean estatus);

}
