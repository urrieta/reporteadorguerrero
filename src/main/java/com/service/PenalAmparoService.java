package com.service;

import java.util.List;
import com.entity.PenalAmparo;

public interface PenalAmparoService {
	public abstract List<PenalAmparo> listAllPenalAmparos();
	public abstract PenalAmparo addPenalAmparo(PenalAmparo entity);
	public abstract void removePenalAmparo(int id);
	public abstract PenalAmparo updatPenalAmparo(PenalAmparo entity);
	public abstract PenalAmparo findById(Integer id);
	public abstract List<PenalAmparo> findByEstatus(boolean estatus);
	public abstract PenalAmparo findByProcesoPenalId(Integer id);
	public abstract PenalAmparo update(PenalAmparo from);

}
