package com.service;

import java.util.List;

import com.entity.Juzgado;

public interface JuzgadoService {
	public abstract List<Juzgado> listAllJuzgados();
	public abstract Juzgado addJuzgado(Juzgado juzgado);
	public abstract void removeJuzgado(int id);
	public abstract Juzgado updatJuzgado(Juzgado juzgado);
	public abstract Juzgado findById(Integer id);
	public abstract List<Juzgado> findByEstatus(boolean estatus);

}
