package com.service;

import java.util.List;

import com.entity.Distrito;

public interface DistritoService {
	public abstract List<Distrito> listAllDistritos();
	public abstract Distrito addDistrito(Distrito distrito);
	public abstract void removeDistrito(int id);
	public abstract Distrito updatDistrito(Distrito distrito);
	public abstract Distrito findById(Integer id);
	public abstract List<Distrito> findByEstatus(boolean estatus);

}
