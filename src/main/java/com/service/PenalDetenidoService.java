package com.service;

import java.util.List;
import com.entity.PenalDetenido;

public interface PenalDetenidoService {
	public abstract List<PenalDetenido> listAllPenalDetenidos();
	public abstract PenalDetenido addPenalDetenido(PenalDetenido entity);
	public abstract void removePenalDetenido(int id);
	public abstract PenalDetenido updatPenalDetenido(PenalDetenido entity);
	public abstract PenalDetenido findById(Integer id);
	public abstract List<PenalDetenido> findByEstatus(boolean estatus);
	public abstract PenalDetenido findByProcesoPenalId(Integer id);

}
