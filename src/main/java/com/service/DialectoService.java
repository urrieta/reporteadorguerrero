package com.service;

import java.util.List;

import com.entity.Dialecto;

public interface DialectoService {
	public abstract List<Dialecto> listAllDialectos();
	public abstract Dialecto addDialecto(Dialecto dialecto);
	public abstract void removeDialecto(int id);
	public abstract Dialecto updatDialecto(Dialecto dialecto);
	public abstract Dialecto findById(Integer id);
	public abstract List<Dialecto> findByEstatus(boolean estatus);

}
