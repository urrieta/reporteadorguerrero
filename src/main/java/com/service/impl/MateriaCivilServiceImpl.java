package com.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.entity.MateriaCivil;
import com.repository.MateriaCivilJpaRepository;
import com.service.MateriaCivilService;

@Service("materiaCivilServiceImpl")
public class MateriaCivilServiceImpl implements MateriaCivilService {
	
	@Autowired
	@Qualifier("materiaCivilJpaRepository")
	private MateriaCivilJpaRepository repositorio;
	
	@PersistenceContext
	EntityManager entityManager = null;

	@Override
	public List<MateriaCivil> listAllMateriaCivil() {
		return repositorio.findAll();
	}

	@Override
	public MateriaCivil findById(Integer id) {
		return  repositorio.findById(id);
	}

	@Override
	public MateriaCivil addMateriaCivil(MateriaCivil entity) {
		return repositorio.save(entity);
	}

	@Override
	public void removeMateriaCivil(int id) {
		repositorio.deleteById(id);
	}

	@Override
	public MateriaCivil updateMateriaCivil(MateriaCivil entity) {
		return repositorio.save(entity);
	}

	@Override
	public List<MateriaCivil> findByEstatus(boolean estatus) {
		return  repositorio.findByEstatus(estatus);
	}
	
	public static Date ParseFecha(String fecha){
        SimpleDateFormat formato = new SimpleDateFormat("MM-dd-yyyy");
        Date fechaDate = null;
        try {
        	if(fecha != null && !fecha.equals("")) {
        		fechaDate = formato.parse(fecha);
        	}
        }catch (ParseException ex){
        }
        return fechaDate;
    }

}
