package com.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.entity.Acusado;
import com.repository.AcusadoJpaRepository;
import com.service.AcusadoService;


@Service("acusadoServiceImpl")
public class AcusadoServiceImpl implements  AcusadoService{
	
	@Autowired
	@Qualifier("acusadoJpaRepository")
	private AcusadoJpaRepository acusadoJpaRepository;
	
	@PersistenceContext
    EntityManager entityManager = null;

	@Override
	public List<Acusado> listAllAcusados() {
		return acusadoJpaRepository.findAll();
	}

	@Override
	public Acusado addAcusado(Acusado acusado) {
		return acusadoJpaRepository.save(acusado);
	}

	@Override
	public void removeAcusado(int id) {
		acusadoJpaRepository.deleteById(id);
	}

	@Override
	public Acusado updatAcusado(Acusado acusado) {
		return acusadoJpaRepository.save(acusado);
	}
	
	@Override
	public Acusado findById(Integer id) {
		return  acusadoJpaRepository.findById(id);
	}
	
	@Override
	public List<Acusado> findByEstatus(boolean estatus) {
		return  acusadoJpaRepository.findByEstatus(estatus);
	}
	
	@Override
	public List<Acusado> findByIdPenalCausaPenal(Integer id) {
		return  null; //acusadoJpaRepository.findByIdPenalCausaPenal(id);
	}
	
}
