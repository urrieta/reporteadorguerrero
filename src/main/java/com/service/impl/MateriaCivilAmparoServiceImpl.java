package com.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.entity.MateriaCivilAmparo;
import com.repository.MateriaCivilAmparoJpaRepository;
import com.service.MateriaCivilAmparoService;

@Service("materiaCivilAmparoServiceImpl")
public class MateriaCivilAmparoServiceImpl implements MateriaCivilAmparoService {
	
	@Autowired
	@Qualifier("materiaCivilAmparoJpaRepository")
	private MateriaCivilAmparoJpaRepository repositorio;
	
	@PersistenceContext
    EntityManager entityManager = null;

	@Override
	public List<MateriaCivilAmparo> listAllMateriaCivilAmparos() {
		return repositorio.findAll();
	}

	@Override
	public MateriaCivilAmparo addMateriaCivilAmparo(MateriaCivilAmparo entity) {
		return repositorio.save(entity);
	}

	@Override
	public void removeMateriaCivilAmparo(int id) {
		repositorio.deleteById(id);
	}

	@Override
	public MateriaCivilAmparo updateMateriaCivilAmparo(MateriaCivilAmparo entity) {
		return repositorio.save(entity);
	}

	@Override
	public MateriaCivilAmparo findById(Integer id) {
		return  repositorio.findById(id);
	}

	@Override
	public List<MateriaCivilAmparo> findByEstatus(boolean estatus) {
		return  repositorio.findByEstatus(estatus);
	}

}
