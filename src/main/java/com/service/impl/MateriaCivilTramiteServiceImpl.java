package com.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.entity.MateriaCivilTramite;
import com.repository.MateriaCivilTramiteJpaRepository;
import com.service.MateriaCivilTramiteService;

@Service("materiaCivilTramiteServiceImpl")
public class MateriaCivilTramiteServiceImpl implements MateriaCivilTramiteService {
	
	@Autowired
	@Qualifier("materiaCivilTramiteJpaRepository")
	private MateriaCivilTramiteJpaRepository repositorio;
	
	@PersistenceContext
    EntityManager entityManager = null;

	@Override
	public List<MateriaCivilTramite> listAllMateriaCivilTramites() {
		return repositorio.findAll();
	}

	@Override
	public MateriaCivilTramite addMateriaCivilTramite(MateriaCivilTramite entity) {
		return repositorio.save(entity);
	}

	@Override
	public void removeMateriaCivilTramite(int id) {
		repositorio.deleteById(id);
	}

	@Override
	public MateriaCivilTramite updateMateriaCivilTramite(MateriaCivilTramite entity) {
		return repositorio.save(entity);
	}

	@Override
	public MateriaCivilTramite findById(Integer id) {
		return  repositorio.findById(id);
	}

	@Override
	public List<MateriaCivilTramite> findByEstatus(boolean estatus) {
		return  repositorio.findByEstatus(estatus);
	}

}
