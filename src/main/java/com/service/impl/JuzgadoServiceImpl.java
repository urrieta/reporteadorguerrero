package com.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.entity.Juzgado;
import com.repository.JuzgadoJpaRepository;
import com.service.JuzgadoService;

@Service("juzgadoServiceImpl")
public class JuzgadoServiceImpl implements  JuzgadoService{
	
	@Autowired
	@Qualifier("juzgadoJpaRepository")
	private JuzgadoJpaRepository juzgadoJpaRepository;
	
	@PersistenceContext
    EntityManager entityManager = null;

	@Override
	public List<Juzgado> listAllJuzgados() {
		return juzgadoJpaRepository.findAll();
	}

	@Override
	public Juzgado addJuzgado(Juzgado juzgado) {
		return juzgadoJpaRepository.save(juzgado);
	}

	@Override
	public void removeJuzgado(int id) {
		juzgadoJpaRepository.deleteById(id);
	}

	@Override
	public Juzgado updatJuzgado(Juzgado juzgado) {
		return juzgadoJpaRepository.save(juzgado);
	}
	
	@Override
	public Juzgado findById(Integer id) {
		return  juzgadoJpaRepository.findById(id);
	}
	
	@Override
	public List<Juzgado> findByEstatus(boolean estatus) {
		return  juzgadoJpaRepository.findByEstatus(estatus);
	}
	
}
