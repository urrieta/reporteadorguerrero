package com.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.entity.Autoridad;
import com.repository.AutoridadJpaRepository;
import com.service.AutoridadService;

@Service("autoridadServiceImpl")
public class AutoridadServiceImpl implements  AutoridadService{
	
	@Autowired
	@Qualifier("autoridadJpaRepository")
	private AutoridadJpaRepository autoridadJpaRepository;
	
	@PersistenceContext
    EntityManager entityManager = null;

	@Override
	public List<Autoridad> listAllAutoridads() {
		return autoridadJpaRepository.findAll();
	}

	@Override
	public Autoridad addAutoridad(Autoridad autoridad) {
		return autoridadJpaRepository.save(autoridad);
	}

	@Override
	public void removeAutoridad(int id) {
		autoridadJpaRepository.deleteById(id);
	}

	@Override
	public Autoridad updatAutoridad(Autoridad autoridad) {
		return autoridadJpaRepository.save(autoridad);
	}
	
	@Override
	public Autoridad findById(Integer id) {
		return  autoridadJpaRepository.findById(id);
	}
	
	@Override
	public List<Autoridad> findByEstatus(boolean estatus) {
		return  autoridadJpaRepository.findByEstatus(estatus);
	}
	
}
