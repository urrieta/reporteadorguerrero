package com.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.entity.PenalAmparo;
import com.repository.PenalAmparoJpaRepository;
import com.service.PenalAmparoService;

@Service("penalAmparoServiceImpl")
public class PenalAmparoServiceImpl implements  PenalAmparoService{
	
	@Autowired
	@Qualifier("penalAmparoJpaRepository")
	private PenalAmparoJpaRepository repositorio;
	
	@PersistenceContext
    EntityManager entityManager = null;

	@Override
	public List<PenalAmparo> listAllPenalAmparos() {
		return repositorio.findAll();
	}

	@Override
	public PenalAmparo addPenalAmparo(PenalAmparo entity) {
		return repositorio.save(entity);
	}

	@Override
	public void removePenalAmparo(int id) {
		repositorio.deleteById(id);
	}


	
	@Override
	public PenalAmparo findById(Integer id) {
		return  repositorio.findById(id);
	}
	
	@Override
	public List<PenalAmparo> findByEstatus(boolean estatus) {
		return  repositorio.findByEstatus(estatus);
	}
	
	@Override
	public PenalAmparo findByProcesoPenalId(Integer id) {
		return  repositorio.getByProcesoPenalId(id);
	}
	
	
	@Override
	public PenalAmparo update(PenalAmparo from) {
		PenalAmparo to = findById(from.getId());
		to = map(from, to);
		return repositorio.save(to);
	}
	
	
	private  PenalAmparo map(PenalAmparo from,PenalAmparo to) {
		try {
			BeanUtils.copyProperties(to, from);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return to;
	}

	@Override
	public PenalAmparo updatPenalAmparo(PenalAmparo entity) {
		// TODO Auto-generated method stub
		return null;
	}
}
