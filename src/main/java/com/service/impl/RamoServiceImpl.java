package com.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.entity.Ramo;
import com.repository.RamoJpaRepository;
import com.service.RamoService;

@Service("ramoServiceImpl")
public class RamoServiceImpl implements  RamoService{
	
	@Autowired
	@Qualifier("ramoJpaRepository")
	private RamoJpaRepository ramoJpaRepository;
	
	@PersistenceContext
    EntityManager entityManager = null;

	@Override
	public List<Ramo> listAllRamos() {
		return ramoJpaRepository.findAll();
	}

	@Override
	public Ramo addRamo(Ramo ramo) {
		return ramoJpaRepository.save(ramo);
	}

	@Override
	public void removeRamo(int id) {
		ramoJpaRepository.deleteById(id);
	}

	@Override
	public Ramo updatRamo(Ramo ramo) {
		return ramoJpaRepository.save(ramo);
	}
	
	@Override
	public Ramo findById(Integer id) {
		return  ramoJpaRepository.findById(id);
	}
	
	@Override
	public List<Ramo> findByEstatus(boolean estatus) {
		return  ramoJpaRepository.findByEstatus(estatus);
	}
	
}
