package com.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.entity.ProcesoPenalFormalPrision;
import com.repository.PenalFormalPrisionJpaRepository;
import com.service.PenalFormalPrisionService;

@Service("penalFormalPrisionServiceImpl")
public class PenalFormalPrisionServiceImpl implements  PenalFormalPrisionService{
	
	@Autowired
	@Qualifier("penalFormalPrisionJpaRepository")
	private PenalFormalPrisionJpaRepository repositorio;
	
	@PersistenceContext
    EntityManager entityManager = null;

	@Override
	public List<ProcesoPenalFormalPrision> listAllProcesoPenalFormalPrision() {
		return repositorio.findAll();
	}

	@Override
	public ProcesoPenalFormalPrision addProcesoPenalFormalPrision(ProcesoPenalFormalPrision entity) {
		return repositorio.save(entity);
	}

	@Override
	public void removeProcesoPenalFormalPrision(int id) {
		repositorio.deleteById(id);
	}

	@Override
	public ProcesoPenalFormalPrision updatProcesoPenalFormalPrision(ProcesoPenalFormalPrision entity) {
		return repositorio.save(entity);
	}
	
	@Override
	public ProcesoPenalFormalPrision findById(Integer id) {
		return  repositorio.findById(id);
	}
	
	@Override
	public ProcesoPenalFormalPrision findByProcesoPenalId(Integer id) {
		return  repositorio.getByProcesoPenalId(id);
	}
}
