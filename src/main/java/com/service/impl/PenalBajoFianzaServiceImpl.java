package com.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.entity.ProcesoPenalBajoFianza;
import com.repository.PenalBajoFianzaJpaRepository;
import com.service.PenalBajoFianzaService;

@Service("penalBajoFianzaServiceImpl")
public class PenalBajoFianzaServiceImpl implements  PenalBajoFianzaService{
	
	@Autowired
	@Qualifier("penalBajoFianzaJpaRepository")
	private PenalBajoFianzaJpaRepository repositorio;
	
	@PersistenceContext
    EntityManager entityManager = null;

	@Override
	public List<ProcesoPenalBajoFianza> listAllProcesoPenalBajoFianza() {
		return repositorio.findAll();
	}

	@Override
	public ProcesoPenalBajoFianza addProcesoPenalBajoFianza(ProcesoPenalBajoFianza entity) {
		return repositorio.save(entity);
	}

	@Override
	public void removeProcesoPenalBajoFianza(int id) {
		repositorio.deleteById(id);
	}

	@Override
	public ProcesoPenalBajoFianza updatProcesoPenalBajoFianza(ProcesoPenalBajoFianza entity) {
		return repositorio.save(entity);
	}
	
	@Override
	public ProcesoPenalBajoFianza findById(Integer id) {
		return  repositorio.findById(id);
	}
	
	
	@Override
	public ProcesoPenalBajoFianza findByProcesoPenalId(Integer id) {
		return  repositorio.getByProcesoPenalId(id);
	}
}
