package com.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.entity.Distrito;
import com.repository.DistritoJpaRepository;
import com.service.DistritoService;

@Service("distritoServiceImpl")
public class DistritoServiceImpl implements  DistritoService{
	
	@Autowired
	@Qualifier("distritoJpaRepository")
	private DistritoJpaRepository distritoJpaRepository;
	
	@PersistenceContext
    EntityManager entityManager = null;

	@Override
	public List<Distrito> listAllDistritos() {
		return distritoJpaRepository.findAll();
	}

	@Override
	public Distrito addDistrito(Distrito Distrito) {
		return distritoJpaRepository.save(Distrito);
	}

	@Override
	public void removeDistrito(int id) {
		distritoJpaRepository.deleteById(id);
	}

	@Override
	public Distrito updatDistrito(Distrito distrito) {
		return distritoJpaRepository.save(distrito);
	}
	
	@Override
	public Distrito findById(Integer id) {
		return  distritoJpaRepository.getById(id);
	}
	
	@Override
	public List<Distrito> findByEstatus(boolean estatus) {
		return  distritoJpaRepository.findByEstatus(estatus);
	}
	
}
