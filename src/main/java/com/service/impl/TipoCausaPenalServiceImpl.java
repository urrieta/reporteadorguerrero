package com.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.entity.TipoCausaPenal;
import com.repository.TipoCausaPenalJpaRepository;
import com.service.TipoCausaPenalService;


@Service("tipoCausaPenalServiceImpl")
public class TipoCausaPenalServiceImpl implements  TipoCausaPenalService{
	
	@Autowired
	@Qualifier("tipoCausaPenalJpaRepository")
	private TipoCausaPenalJpaRepository tipoCausaPenalJpaRepository;
	
	@PersistenceContext
    EntityManager entityManager = null;

	@Override
	public List<TipoCausaPenal> listAll() {
		return tipoCausaPenalJpaRepository.findAll();
	}

	@Override
	public TipoCausaPenal addTipoCausaPenal(TipoCausaPenal entidad) {
		return tipoCausaPenalJpaRepository.save(entidad);
	}

	@Override
	public void removeAcusado(int id) {
		tipoCausaPenalJpaRepository.deleteById(id);
	}

	@Override
	public TipoCausaPenal updatTipoCausaPenal(TipoCausaPenal entidad) {
		return tipoCausaPenalJpaRepository.save(entidad);
	}
	
	@Override
	public TipoCausaPenal findById(Integer id) {
		return  tipoCausaPenalJpaRepository.findById(id);
	}
	
	@Override
	public List<TipoCausaPenal> findByEstatus(boolean estatus) {
		return null;// tipoCausaPenalJpaRepository.findByEstatus(estatus);
	}
	
	
}
