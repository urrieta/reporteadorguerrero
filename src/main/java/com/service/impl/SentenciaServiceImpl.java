package com.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.entity.TipoSentencia;
import com.repository.SentenciaJpaRepository;
import com.service.SentenciaService;

@Service("sentenciaServiceImpl")
public class SentenciaServiceImpl implements  SentenciaService{
	
	@Autowired
	@Qualifier("sentenciaJpaRepository")
	private SentenciaJpaRepository sentenciaJpaRepository;
	
	@PersistenceContext
    EntityManager entityManager = null;

	@Override
	public List<TipoSentencia> listAll() {
		return sentenciaJpaRepository.findAll();
	}

	@Override
	public TipoSentencia add(TipoSentencia entity) {
		return sentenciaJpaRepository.save(entity);
	}

	@Override
	public void remove(int id) {
		sentenciaJpaRepository.deleteById(id);
	}

	@Override
	public TipoSentencia updat(TipoSentencia delito) {
		return sentenciaJpaRepository.save(delito);
	}
	
	@Override
	public TipoSentencia findById(Integer id) {
		return  sentenciaJpaRepository.findById(id);
	}
	
	@Override
	public List<TipoSentencia> findByEstatus(boolean estatus) {
		return  null;
	}
	
}
