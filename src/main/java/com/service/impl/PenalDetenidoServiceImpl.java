package com.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.entity.PenalDetenido;
import com.repository.PenalDetenidoJpaRepository;
import com.service.PenalDetenidoService;

@Service("penalDetenidoServiceImpl")
public class PenalDetenidoServiceImpl implements  PenalDetenidoService{
	
	@Autowired
	@Qualifier("penalDetenidoJpaRepository")
	private PenalDetenidoJpaRepository repositorio;
	
	@PersistenceContext
    EntityManager entityManager = null;

	@Override
	public List<PenalDetenido> listAllPenalDetenidos() {
		return repositorio.findAll();
	}

	@Override
	public PenalDetenido addPenalDetenido(PenalDetenido entity) {
		return repositorio.save(entity);
	}

	@Override
	public void removePenalDetenido(int id) {
		repositorio.deleteById(id);
	}

	@Override
	public PenalDetenido updatPenalDetenido(PenalDetenido entity) {
		return repositorio.save(entity);
	}
	
	@Override
	public PenalDetenido findById(Integer id) {
		return  repositorio.findById(id);
	}

	@Override
	public List<PenalDetenido> findByEstatus(boolean estatus) {
		// TODO Auto-generated method stub
		return null;
	}
	

	@Override
	public PenalDetenido findByProcesoPenalId(Integer id) {
		return  repositorio.getByProcesoPenalId(id);
	}
}
