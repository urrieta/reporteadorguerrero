package com.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.entity.PenalCausasTramite;
import com.repository.PenalCausasTramiteJpaRepository;
import com.service.PenalCausasTramiteService;

@Service("penalCausasTramiteServiceImpl")
public class PenalCausasTramiteServiceImpl implements  PenalCausasTramiteService{
	
	@Autowired
	@Qualifier("penalCausasTramiteJpaRepository")
	private PenalCausasTramiteJpaRepository repositorio;
	
	@PersistenceContext
    EntityManager entityManager = null;

	@Override
	public List<PenalCausasTramite> listAllPenalCausasTramites() {
		return repositorio.findAll();
	}

	@Override
	public PenalCausasTramite addPenalCausasTramite(PenalCausasTramite entity) {
		return repositorio.save(entity);
	}

	@Override
	public void removePenalCausasTramite(int id) {
		repositorio.deleteById(id);
	}

	@Override
	public PenalCausasTramite updatPenalCausasTramite(PenalCausasTramite entity) {
		return repositorio.save(entity);
	}
	
	@Override
	public PenalCausasTramite findById(Integer id) {
		return  repositorio.findById(id);
	}
	
	@Override
	public List<PenalCausasTramite> findByEstatus(boolean estatus) {
		return  repositorio.findByEstatus(estatus);
	}
	
}
