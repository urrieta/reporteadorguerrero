package com.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.entity.Municipio;
import com.repository.DistritoJpaRepository;
import com.repository.MunicipioJpaRepository;
import com.service.MunicipioService;

@Service("municipioServiceImpl")
public class MunicipioServiceImpl implements  MunicipioService{
	
	@Autowired
	@Qualifier("municipioJpaRepository")
	private MunicipioJpaRepository municipioJpaRepository;
	
	@Autowired
	@Qualifier("distritoJpaRepository")
	private DistritoJpaRepository distritoJpaRepository;
	
	@PersistenceContext
    EntityManager entityManager = null;

	@Override
	public List<Municipio> listAllMunicipios() {
		return municipioJpaRepository.findAll();
	}

	@Override
	public Municipio addMunicipio(Municipio municipio) {
		return municipioJpaRepository.save(municipio);
	}

	@Override
	public void removeMunicipio(int id) {
		municipioJpaRepository.deleteById(id);
	}

	@Override
	public Municipio updatMunicipio(Municipio municipio) {
		return municipioJpaRepository.save(municipio);
	}
	
	@Override
	public Municipio findById(Integer id) {
		return  municipioJpaRepository.findById(id);
	}
	
	@Override
	public List<Municipio> findByEstatus(boolean estatus) {
		return  municipioJpaRepository.findByEstatus(estatus);
	}
	
}
