package com.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.entity.Agraviado;
import com.repository.AgraviadoJpaRepository;
import com.service.AgraviadoService;


@Service("agraviadoServiceImpl")
public class AgraviadoServiceImpl implements  AgraviadoService{
	
	@Autowired
	@Qualifier("agraviadoJpaRepository")
	private AgraviadoJpaRepository agraviadoJpaRepository;
	
	@PersistenceContext
    EntityManager entityManager = null;

	@Override
	public List<Agraviado> listAllAgraviados() {
		return agraviadoJpaRepository.findAll();
	}

	@Override
	public Agraviado addAgraviado(Agraviado agraviado) {
		return agraviadoJpaRepository.save(agraviado);
	}

	@Override
	public void removeAgraviado(int id) {
		agraviadoJpaRepository.deleteById(id);
	}

	@Override
	public Agraviado updatAgraviado(Agraviado agraviado) {
		return agraviadoJpaRepository.save(agraviado);
	}
	
	@Override
	public Agraviado findById(Integer id) {
		return  agraviadoJpaRepository.findById(id);
	}
	
	@Override
	public List<Agraviado> findByEstatus(boolean estatus) {
		return  agraviadoJpaRepository.findByEstatus(estatus);
	}
	
	@Override
	public List<Agraviado> findByIdPenalCausaPenal(Integer id) {
		return null; // agraviadoJpaRepository.findByIdPenalCausaPenal(id);
	}
}
