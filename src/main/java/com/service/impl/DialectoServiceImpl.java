package com.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.entity.Dialecto;
import com.repository.DialectoJpaRepository;
import com.service.DialectoService;

@Service("dialectoServiceImpl")
public class DialectoServiceImpl implements  DialectoService{
	
	@Autowired
	@Qualifier("dialectoJpaRepository")
	private DialectoJpaRepository dialectoJpaRepository;
	
	@PersistenceContext
    EntityManager entityManager = null;

	@Override
	public List<Dialecto> listAllDialectos() {
		return dialectoJpaRepository.findAll();
	}

	@Override
	public Dialecto addDialecto(Dialecto dialecto) {
		return dialectoJpaRepository.save(dialecto);
	}

	@Override
	public void removeDialecto(int id) {
		dialectoJpaRepository.deleteById(id);
	}

	@Override
	public Dialecto updatDialecto(Dialecto dialecto) {
		return dialectoJpaRepository.save(dialecto);
	}
	
	@Override
	public Dialecto findById(Integer id) {
		return  dialectoJpaRepository.findById(id);
	}
	
	@Override
	public List<Dialecto> findByEstatus(boolean estatus) {
		return  dialectoJpaRepository.findByEstatus(estatus);
	}
	
}
