package com.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dto.ReporteAdminPenalUnoDTO;
import com.entity.CausaPenal;
import com.repository.PenalCausaPenalJpaRepository;
import com.service.PenalCausaPenalService;

@Service("penalCausaPenalServiceImpl")
public class PenalCausaPenalServiceImpl implements  PenalCausaPenalService{
	
	@Autowired
	@Qualifier("penalCausaPenalJpaRepository")
	private PenalCausaPenalJpaRepository repositorio;
	
	@PersistenceContext
    EntityManager entityManager = null;

	@Override
	public List<CausaPenal> listAllPenalCausaPenals() {
		return repositorio.findAll();
	}

	@Override
	public CausaPenal addPenalCausaPenal(CausaPenal entity) {
		return repositorio.save(entity);
	}

	@Override
	public void removePenalCausaPenal(int id) {
		repositorio.deleteById(id);
	}

	
	@Override
	public CausaPenal findById(Integer id) {
		return  repositorio.findById(id);
	}
	

	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ReporteAdminPenalUnoDTO> reporteAdminPenalUno() {  
		List<ReporteAdminPenalUnoDTO>  reporte = null;
		 List<Object[]> lista = entityManager.createNativeQuery("select "
		 		    + "d.nombre as nombreDistrito, "
			 		+ "pcp.fecha_inicio,  "
			 		+ "ac.nombre nombreAcusado, "  
			 		+ "ac.apellido_paterno acusadoPaterno,   " 
			 		+ "ac.apellido_materno acusadoMaterno,   " 
			 		+ "ac.edad acusadoEdad,    "
	                + "dia.nombre dialectoAcusado,   "
			 		+ "ag.nombre agraviadoNombre,    "
			 		+ "ag.apellido_paterno agraviadoPaterno,    "
			 		+ "ag.apellido_materno agraviadoMaterno,  "  
			 		+ "ag.edad agraviadoEdad,    "
	                + "dia2.nombre agraviadoDialecto,   "
                    + "ppfp.fecha_apelacion as aprension_liberacion,   " 
			 		+ "ppfp.fecha_apelacion as aprension_negacion,  "  
			 		+ "ppfp.fecha_apelacion  as aprension_ejecucion,   "
			 		+ "pcp.cd,    "
			 		+ "pcp.sc,    "
	                + "deli.nombre delito,   "
			 		+ "pcp.id    "
			 		+ "from    "
			 		+ "causa_penal pcp    "
	                + "LEFT JOIN   "
                    + "proceso_penal pp "
                    + "on "
                    + "pp.id_causa_penal = pcp.id "
                    + "INNER JOIN   "
	                + "acusado ac   "
	                + "on   "
	                + "ac.id = pcp.id_acusado  "
	                + "INNER JOIN   "
	                + "dialecto dia   "
	                + "on   "
	                + "dia.id = ac.id_dialecto   "
	                + "INNER JOIN   "
	                + "agraviado ag   "
	                + "on   "
	                + "ag.id = pcp.id_agraviado  "
				    + "LEFT JOIN   "
	                + "dialecto dia2   "
	                + "on   "
	                + "dia2.id = ag.id_dialecto   "
	                + "INNER JOIN    "
	                + "delito deli   "
	                + "on   "
	                + "deli.id = ac.id_delito "
                    + "INNER JOIN    "
			 		+ "juzgado J    "
			 		+ "ON    "
			 		+ "pcp.id_juzgado = j.id    "
			 		+ "inner join    "
			 		+ "distrito d    "
			 		+ "on    "
			 		+ "j.id_distrito =  d.id    "
                    + "LEFT JOIN  "
                    + "proceso_penal_formal_prision ppfp "
                    + "on "
                    + "ppfp.id_proceso_penal = pp.id ").getResultList();
		 
		 if(!lista.isEmpty()) {
			 reporte = new ArrayList<> ();
			 for(Object[] obj : lista) {
				 ReporteAdminPenalUnoDTO dto = new ReporteAdminPenalUnoDTO();
				 dto.setNombreDistrito(String.valueOf(obj[0]));
				 dto.setFechaInicio(ParseFecha(String.valueOf(obj[1])));
				 dto.setAcusadoNombre(String.valueOf(obj[2]));
				 dto.setAcusadoApellidoPaterno(String.valueOf(obj[3]));
				 dto.setAcusadoApellidoMaterno(String.valueOf(obj[4]));
				 dto.setEdadAcusado(Integer.parseInt(String.valueOf(obj[5])));
				 
				 dto.setNombreLenguaAcusado(String.valueOf(obj[6]));
				 
				 dto.setAgraviadoNombre(String.valueOf(obj[7]));
				 dto.setAgraviadoApellidoPaterno(String.valueOf(obj[8]));
				 dto.setAgraviadoApellidoMaterno(String.valueOf(obj[9]));
				 dto.setEdadAgraviado(Integer.parseInt(String.valueOf(obj[10])));
				 dto.setNombreLenguaAgraviado(String.valueOf(obj[11]));
				 dto.setFechaAprensionLiberacion(obj[12] != null?ParseFecha(String.valueOf(obj[12])):null);
				 dto.setFechaAprensionNegacion(obj[13]!= null ?ParseFecha(String.valueOf(obj[13] )):null);
				 dto.setFechaAprensionEjecucion( obj[14] != null ? ParseFecha(String.valueOf(obj[14])):null);
				 
				 dto.setCdC( String.valueOf(obj[15]) != null ?Boolean.parseBoolean(String.valueOf(obj[15]))? "SI" : "NO" :"-");
				 dto.setScC( String.valueOf(obj[15]) != null ?Boolean.parseBoolean(String.valueOf(obj[16]))? "SI" : "NO" :"-");
				 
				 dto.setNombreDelito( obj[17] != null ? String.valueOf(obj[17]):null);
				 dto.setId(Integer.parseInt(String.valueOf(obj[18])));
				 dto.setCarpetaJudicial("P-11/2021");
				 reporte.add(dto);

			 }
		 }
		 return reporte;
	}
	
	public static Date ParseFecha(String fecha){
        SimpleDateFormat formato = new SimpleDateFormat("MM-dd-yyyy");
        Date fechaDate = null;
        try {
        	if(fecha != null && !fecha.equals("")) {
        		fechaDate = formato.parse(fecha);
        	}
        }catch (ParseException ex){
        }
        return fechaDate;
    }

	@Override
	public CausaPenal updatePenal(CausaPenal from) {
		CausaPenal to = findById(from.getId());
		to = mapPenal(from, to);
		return repositorio.save(to);
	}
	
	
	private  CausaPenal mapPenal(CausaPenal from,CausaPenal to) {
		to.setRadicada(from.isRadicada());
		try {
			BeanUtils.copyProperties(to, from);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return to;
	}

	@Override
	public List<CausaPenal> findByEstatus(boolean estatus) {
		// TODO Auto-generated method stub
		return null;
	}

}
