package com.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.entity.Delito;
import com.repository.DelitoJpaRepository;
import com.service.DelitoService;

@Service("delitoServiceImpl")
public class DelitoServiceImpl implements  DelitoService{
	
	@Autowired
	@Qualifier("delitoJpaRepository")
	private DelitoJpaRepository delitoJpaRepository;
	
	@PersistenceContext
    EntityManager entityManager = null;

	@Override
	public List<Delito> listAllDelitos() {
		return delitoJpaRepository.findAll();
	}

	@Override
	public Delito addDelito(Delito delito) {
		return delitoJpaRepository.save(delito);
	}

	@Override
	public void removeDelito(int id) {
		delitoJpaRepository.deleteById(id);
	}

	@Override
	public Delito updatDelito(Delito delito) {
		return delitoJpaRepository.save(delito);
	}
	
	@Override
	public Delito findById(Integer id) {
		return  delitoJpaRepository.findById(id);
	}
	
	@Override
	public List<Delito> findByEstatus(boolean estatus) {
		return  delitoJpaRepository.findByEstatus(estatus);
	}
	
}
