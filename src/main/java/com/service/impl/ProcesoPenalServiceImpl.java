package com.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.entity.ProcesoPenal;
import com.repository.ProcesoPenalJpaRepository;
import com.service.ProcesoPenalService;

@Service("procesoPenalServiceImpl")
public class ProcesoPenalServiceImpl implements  ProcesoPenalService{
	
	@Autowired
	@Qualifier("procesoPenalJpaRepository")
	private ProcesoPenalJpaRepository repositorio;
	
	@PersistenceContext
    EntityManager entityManager = null;

	@Override
	public List<ProcesoPenal> listAllProcesoPenal() {
		return repositorio.findAll();
	}

	@Override
	public ProcesoPenal addProcesoPenal(ProcesoPenal entity) {
		return repositorio.save(entity);
	}

	@Override
	public void removeProcesoPenal(int id) {
		repositorio.deleteById(id);
	}

	@Override
	public ProcesoPenal findById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ProcesoPenal> findByEstatus(boolean estatus) {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public ProcesoPenal findByIdCausaPenal(Integer id) {
		return repositorio.findByIdCausaPenal(id);
	}
	
	
	@Override
	public ProcesoPenal update(ProcesoPenal from) {
		ProcesoPenal to = findById(from.getId());
		to = map(from, to);
		return repositorio.save(to);
	}
	
	
	private  ProcesoPenal map(ProcesoPenal from,ProcesoPenal to) {
		try {
			BeanUtils.copyProperties(to, from);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return to;
	}

}
