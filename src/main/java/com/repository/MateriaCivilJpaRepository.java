package com.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.MateriaCivil;

@Repository("materiaCivilJpaRepository")
public interface MateriaCivilJpaRepository extends JpaRepository<MateriaCivil, Serializable> {
	 MateriaCivil findById(Integer id);
	 
	 List<MateriaCivil> findByEstatus(boolean estatus);
}
