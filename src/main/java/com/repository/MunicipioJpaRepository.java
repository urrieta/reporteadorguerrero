package com.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.Municipio;

@Repository("municipioJpaRepository")
public interface MunicipioJpaRepository extends JpaRepository<Municipio, Serializable> {
	Municipio findById(Integer id);
	
	List<Municipio> findByEstatus(boolean estatus);


}
