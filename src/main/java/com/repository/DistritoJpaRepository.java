package com.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.Distrito;

@Repository("distritoJpaRepository")
public interface DistritoJpaRepository extends JpaRepository<Distrito, Serializable> {
	
	List<Distrito> findByEstatus(boolean estatus);


}
