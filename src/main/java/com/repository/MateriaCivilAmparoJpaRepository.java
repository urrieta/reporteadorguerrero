package com.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import com.entity.MateriaCivilAmparo;

@Repository("materiaCivilAmparoJpaRepository")
public interface MateriaCivilAmparoJpaRepository extends JpaRepository<MateriaCivilAmparo, Serializable> {
	MateriaCivilAmparo findById(Integer id);
	
	List<MateriaCivilAmparo> findByEstatus(boolean estatus);
}
