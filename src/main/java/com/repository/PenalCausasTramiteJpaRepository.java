package com.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.PenalCausasTramite;

@Repository("penalCausasTramiteJpaRepository")
public interface PenalCausasTramiteJpaRepository extends JpaRepository<PenalCausasTramite, Serializable> {
	PenalCausasTramite findById(Integer id);
	
	List<PenalCausasTramite> findByEstatus(boolean estatus);


}
