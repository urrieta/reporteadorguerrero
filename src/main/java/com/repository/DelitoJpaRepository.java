package com.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.Delito;

@Repository("delitoJpaRepository")
public interface DelitoJpaRepository extends JpaRepository<Delito, Serializable> {
	Delito findById(Integer id);
	
	List<Delito> findByEstatus(boolean estatus);


}
