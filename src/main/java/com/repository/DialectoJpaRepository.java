package com.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.Dialecto;

@Repository("dialectoJpaRepository")
public interface DialectoJpaRepository extends JpaRepository<Dialecto, Serializable> {
	Dialecto findById(Integer id);
	
	List<Dialecto> findByEstatus(boolean estatus);


}
