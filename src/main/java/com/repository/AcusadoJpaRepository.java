package com.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.Acusado;

@Repository("acusadoJpaRepository")
public interface AcusadoJpaRepository extends JpaRepository<Acusado, Serializable> {
	Acusado findById(Integer id);
	
	List<Acusado> findByEstatus(boolean estatus);

	//List<Acusado> findByIdPenalCausaPenal(Integer id);
}
