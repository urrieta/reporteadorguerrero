package com.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.MateriaCivilTramite;

@Repository("materiaCivilTramiteJpaRepository")
public interface MateriaCivilTramiteJpaRepository extends JpaRepository<MateriaCivilTramite, Serializable> {
	MateriaCivilTramite findById(Integer id);
	
	List<MateriaCivilTramite> findByEstatus(boolean estatus);

}
