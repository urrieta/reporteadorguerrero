package com.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.entity.ProcesoPenalBajoFianza;

@Repository("penalBajoFianzaJpaRepository")
public interface PenalBajoFianzaJpaRepository extends JpaRepository<ProcesoPenalBajoFianza, Serializable> {
	
	ProcesoPenalBajoFianza findById(Integer id);
	@Query(value="select * from proceso_penal_bajo_fianza a where a.id_proceso_penal= :id", nativeQuery=true)
	ProcesoPenalBajoFianza getByProcesoPenalId(Integer id);
}
