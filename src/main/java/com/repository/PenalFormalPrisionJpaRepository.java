package com.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.entity.ProcesoPenalBajoFianza;
import com.entity.ProcesoPenalFormalPrision;

@Repository("penalFormalPrisionJpaRepository")
public interface PenalFormalPrisionJpaRepository extends JpaRepository<ProcesoPenalFormalPrision, Serializable> {
	
	ProcesoPenalFormalPrision findById(Integer id);
	@Query(value="select * from proceso_penal_formal_prision a where a.id_proceso_penal= :id", nativeQuery=true)
	ProcesoPenalFormalPrision getByProcesoPenalId(Integer id);
}
