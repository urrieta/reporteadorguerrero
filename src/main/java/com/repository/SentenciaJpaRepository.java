package com.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.TipoSentencia;

@Repository("sentenciaJpaRepository")
public interface SentenciaJpaRepository extends JpaRepository<TipoSentencia, Serializable> {
	TipoSentencia findById(Integer id);
	


}
