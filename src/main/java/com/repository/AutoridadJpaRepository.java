package com.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.Autoridad;

@Repository("autoridadJpaRepository")
public interface AutoridadJpaRepository extends JpaRepository<Autoridad, Serializable> {
	Autoridad findById(Integer id);
	
	List<Autoridad> findByEstatus(boolean estatus);


}
