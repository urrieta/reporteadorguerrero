package com.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.CausaPenal;

@Repository("penalCausaPenalJpaRepository")
public interface PenalCausaPenalJpaRepository extends JpaRepository<CausaPenal, Serializable> {
	CausaPenal findById(Integer id);
	


}
