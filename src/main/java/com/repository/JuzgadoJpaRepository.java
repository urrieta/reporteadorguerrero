package com.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.Juzgado;

@Repository("juzgadoJpaRepository")
public interface JuzgadoJpaRepository extends JpaRepository<Juzgado, Serializable> {
	Juzgado findById(Integer id);
	
	List<Juzgado> findByEstatus(boolean estatus);


}
