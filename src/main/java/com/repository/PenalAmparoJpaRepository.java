package com.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.entity.PenalAmparo;

@Repository("penalAmparoJpaRepository")
public interface PenalAmparoJpaRepository extends JpaRepository<PenalAmparo, Serializable> {
	PenalAmparo findById(Integer id);
	
	List<PenalAmparo> findByEstatus(boolean estatus);
	//PenalAmparo findByProcesoPenalId(Integer id);
	
	@Query(value="select * from PENAL_AMPARO a where a.proceso_penal_id= :id", nativeQuery=true)
    PenalAmparo getByProcesoPenalId(Integer id);

}
