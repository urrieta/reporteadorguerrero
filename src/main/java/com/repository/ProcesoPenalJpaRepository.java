package com.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.ProcesoPenal;

@Repository("procesoPenalJpaRepository")
public interface ProcesoPenalJpaRepository extends JpaRepository<ProcesoPenal, Serializable> {
	ProcesoPenal findByIdCausaPenal(Integer id);



}
