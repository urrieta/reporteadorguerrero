package com.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.entity.PenalDetenido;

@Repository("penalDetenidoJpaRepository")
public interface PenalDetenidoJpaRepository extends JpaRepository<PenalDetenido, Serializable> {
	PenalDetenido findById(Integer id);
	@Query(value="select * from penal_detenido a where a.id_proceso_penal= :id", nativeQuery=true)
	PenalDetenido getByProcesoPenalId(Integer id);
	


}
