package com.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.TipoCausaPenal;

@Repository("tipoCausaPenalJpaRepository")
public interface TipoCausaPenalJpaRepository extends JpaRepository<TipoCausaPenal, Serializable> {
	TipoCausaPenal findById(Integer id);
	
	//List<TipoCausaPenal> findByEstatus(boolean estatus);

}
