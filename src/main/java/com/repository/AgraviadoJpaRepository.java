package com.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.Agraviado;

@Repository("agraviadoJpaRepository")
public interface AgraviadoJpaRepository extends JpaRepository<Agraviado, Serializable> {
	Agraviado findById(Integer id);
	
	List<Agraviado> findByEstatus(boolean estatus);

	//List<Agraviado> findByIdPenalCausaPenal(Integer id);

}
