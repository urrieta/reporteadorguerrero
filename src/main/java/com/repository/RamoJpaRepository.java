package com.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.Ramo;

@Repository("ramoJpaRepository")
public interface RamoJpaRepository extends JpaRepository<Ramo, Serializable> {
	Ramo findById(Integer id);
	
	List<Ramo> findByEstatus(boolean estatus);


}
