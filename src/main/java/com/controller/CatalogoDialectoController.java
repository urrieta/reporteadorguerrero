package com.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.entity.Dialecto;
import com.service.DialectoService;

@Controller
@RequestMapping("/catalogoDialecto")
public class CatalogoDialectoController {
	
	
	@Autowired
	@Qualifier("dialectoServiceImpl")
	private DialectoService dialectoService;
		
	@GetMapping("/inicio")
	public ModelAndView listAllCursos() {
		ModelAndView mv =  new ModelAndView("catalogoDialecto");
		return mv;
	}
	
	
	@PostMapping("/add")
	@ResponseBody
	public List<Dialecto> add(@RequestBody Dialecto dialecto) {
		dialecto.setEstatus(true);
		dialectoService.addDialecto(dialecto);
		return dialectoService.listAllDialectos();
	}
	
	@GetMapping("/dialecto/{id}")
	@ResponseBody
	public List<Dialecto> Dialecto(@PathVariable("id") Integer id) {
		List<Dialecto> lista = new ArrayList<>();
		lista.add(dialectoService.findById(id));
		return  lista;
	}
	
	
	
	@GetMapping("/dialecto/del/{id}")
	@ResponseBody
	public void del(@PathVariable("id") Integer id) {
		Dialecto Dialecto = dialectoService.findById(id);
		Dialecto.setEstatus(false);
		dialectoService.updatDialecto(Dialecto);
	}
	
	
	@GetMapping("/all/")
	@ResponseBody
	public List<Dialecto> all() {
		return dialectoService.findByEstatus(true);
	}
	
}
