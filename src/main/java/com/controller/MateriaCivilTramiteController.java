package com.controller;

import java.util.ArrayList;
import java.util.List;

import com.service.MateriaCivilTramiteService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.entity.MateriaCivilTramite;

@Controller
@RequestMapping("/materiaCivilTramite")
public class MateriaCivilTramiteController {
	@Autowired
	@Qualifier("materiaCivilTramiteServiceImpl")
	private MateriaCivilTramiteService materiaCivilTramiteService;

	@GetMapping("/all/")
	@ResponseBody
	public List<MateriaCivilTramite> all() {
		return materiaCivilTramiteService.findByEstatus(true);
	}

	@GetMapping("/tramites")
	public ModelAndView listMateriaCivilAmparo() {
		ModelAndView mv = new ModelAndView("materiaCivilTramite");
		return mv;
	}

	@PostMapping("/create")
	@ResponseBody
	public List<MateriaCivilTramite> create(@RequestBody MateriaCivilTramite materiaCivilTramite) {
		materiaCivilTramite.setEstatus(true);
		if (materiaCivilTramite.getId() != null) {
			materiaCivilTramiteService.updateMateriaCivilTramite(materiaCivilTramite);
		} else {
			materiaCivilTramiteService.addMateriaCivilTramite(materiaCivilTramite);
		}
		
		return materiaCivilTramiteService.listAllMateriaCivilTramites();
	}
	
	@GetMapping("/materiaCivil/{id}")
	@ResponseBody
	public List<MateriaCivilTramite> materiaCivil(@PathVariable("id") Integer id) {
		List<MateriaCivilTramite> lista = new ArrayList<MateriaCivilTramite>();
		
		MateriaCivilTramite mc = materiaCivilTramiteService.findById(id);
		
		lista.add(mc);
		
		return lista;
	}
	
	
	
	@GetMapping("/delete/{id}")
	@ResponseBody
	public void delete(@PathVariable("id") Integer id) {
		MateriaCivilTramite materiaCivilTramite = materiaCivilTramiteService.findById(id);
		materiaCivilTramite.setEstatus(false);
		materiaCivilTramiteService.updateMateriaCivilTramite(materiaCivilTramite);
	}

}
