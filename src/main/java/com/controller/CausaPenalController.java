package com.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.dto.CausaPenalVO;
import com.dto.ProcesoPenalVO;
import com.entity.Acusado;
import com.entity.Agraviado;
import com.entity.Autoridad;
import com.entity.Delito;
import com.entity.Dialecto;
import com.entity.PenalAmparo;
import com.entity.PenalDetenido;
import com.entity.ProcesoPenal;
import com.entity.ProcesoPenalBajoFianza;
import com.entity.ProcesoPenalFormalPrision;
import com.entity.TipoSentencia;
import com.entity.CausaPenal;
import com.service.AcusadoService;
import com.service.AgraviadoService;
import com.service.AutoridadService;
import com.service.DelitoService;
import com.service.DialectoService;
import com.service.JuzgadoService;
import com.service.PenalAmparoService;
import com.service.PenalBajoFianzaService;
import com.service.PenalCausaPenalService;
import com.service.PenalDetenidoService;
import com.service.PenalFormalPrisionService;
import com.service.ProcesoPenalService;
import com.service.SentenciaService;

@Controller
@RequestMapping("/causaPenal")
public class CausaPenalController {
	
	@Autowired
	@Qualifier("penalCausaPenalServiceImpl")
	private PenalCausaPenalService penalCausaPenalService;
	
	@Autowired
	@Qualifier("delitoServiceImpl")
	private DelitoService delitoService;
	
	@Autowired
	@Qualifier("juzgadoServiceImpl")
	private JuzgadoService juzgadoService;
	
	@Autowired
	@Qualifier("dialectoServiceImpl")
	private DialectoService dialectoService;
	
	@Autowired
	@Qualifier("sentenciaServiceImpl")
	private SentenciaService sentenciaService;
	
	@Autowired
	@Qualifier("agraviadoServiceImpl")
	private AgraviadoService agraviadoService;
	
	@Autowired
	@Qualifier("autoridadServiceImpl")
	private AutoridadService autoridadService;
	
	@Autowired
	@Qualifier("acusadoServiceImpl")
	private AcusadoService acusadoService;
	
	@Autowired
	@Qualifier("procesoPenalServiceImpl")
	private ProcesoPenalService procesoPenalService;

	@Autowired
	@Qualifier("penalAmparoServiceImpl")
	private PenalAmparoService penalAmparoService;
	
	@Autowired
	@Qualifier("penalDetenidoServiceImpl")
	private PenalDetenidoService penalDetenidoService;
	
	@Autowired
	@Qualifier("penalBajoFianzaServiceImpl")
	private PenalBajoFianzaService penalBajoFianzaService;
	
	@Autowired
	@Qualifier("penalFormalPrisionServiceImpl")
	private PenalFormalPrisionService penalFormalPrisionService;
	
	@GetMapping("/inicio")
	public ModelAndView listAllCursos() {
		ModelAndView mv =  new ModelAndView("causaPenal");
		return mv;
	}
	
	@PostMapping("/add")
	@ResponseBody
	public List<CausaPenal> add(@RequestBody CausaPenalVO penalCausaPenal) {
		
		CausaPenal cp =  new CausaPenal();
		cp.setAcusado(acusadoService.findById((int)(long)penalCausaPenal.getAcusado()));
		cp.setAgraviado(agraviadoService.findById((int)(long)penalCausaPenal.getAgraviado()));
		Delito delito = delitoService.findById((int)(long)penalCausaPenal.getDelito());
		cp.getAcusado().setDelito(delito);
		cp.setAveriguacionPrevia(penalCausaPenal.getAveriguacionPrevia());
		cp.setCausaPenal(penalCausaPenal.getCausaPenal());
		cp.setFechaInicio(penalCausaPenal.getFechaInicio());
		cp.setCd(penalCausaPenal.isCd());
		cp.setSd(penalCausaPenal.isSd());
		cp.setObservaciones(penalCausaPenal.getObservaciones());
		cp.setRadicada(penalCausaPenal.isRadicada());
		cp.setJuzgado(juzgadoService.listAllJuzgados().get(0));
		
		if( penalCausaPenal.getId() != null) {
			cp.setId(penalCausaPenal.getId());
			penalCausaPenalService.updatePenal(cp);
		}else {
			penalCausaPenalService.addPenalCausaPenal(cp);
		}
		
		return penalCausaPenalService.listAllPenalCausaPenals();
	}
	
	@GetMapping("/penalCausaPenal/{id}")
	@ResponseBody
	public List<CausaPenal> PenalCausaPenal(@PathVariable("id") Integer id) {
		List<CausaPenal> lista = new ArrayList<>();
		CausaPenal causaPenal = penalCausaPenalService.findById(id);
		lista.add(causaPenal);
		return  lista;
	}
	
	
	@GetMapping("/penalCausaPenal/del/{id}")
	@ResponseBody
	public void del(@PathVariable("id") Integer id) {
		CausaPenal penalCausaPenal = penalCausaPenalService.findById(id);
		penalCausaPenalService.updatePenal(penalCausaPenal);
	}
	
	
	@GetMapping("/allPenalCausaPenales/")
	@ResponseBody
	public List<CausaPenal> all() {
		return penalCausaPenalService.listAllPenalCausaPenals();
	}
	
	
	@GetMapping("/delitos/")
	@ResponseBody
	public List<Delito> allDelitos() {
		return delitoService.listAllDelitos();
	}
	
	
	@GetMapping("/dialectos/")
	@ResponseBody
	public List<Dialecto> allDialectos() {
		return dialectoService.listAllDialectos();
	}
	
	
	@GetMapping("/agraviados/")
	@ResponseBody
	public List<Agraviado> allAgraviados() {
		return agraviadoService.listAllAgraviados();
	}
	
	@GetMapping("/acusados/")
	@ResponseBody
	public List<Acusado> allAcuados() {
		return acusadoService.listAllAcusados();
	}

	public static Date ParseFecha(String fecha){
        SimpleDateFormat formato = new SimpleDateFormat("MM/dd/yyyy");
        Date fechaDate = null;
        try {
        	if(fecha != null && !fecha.equals("")) {
        		fechaDate = formato.parse(fecha);
        	}
        }catch (ParseException ex){
        }
        return fechaDate;
    }
	
	public static Date ParseFechaAct(String fecha){
        SimpleDateFormat formato = new SimpleDateFormat("MM-dd-yyyy");
        Date fechaDate = null;
        try {
        	if(fecha != null && !fecha.equals("")) {
        		fechaDate = formato.parse(fecha);
        	}
        }catch (ParseException ex){
        }
        return fechaDate;
    }
	
	
	@GetMapping("/penalCausaPenalProceso/{id}")
	@ResponseBody
	public List<ProcesoPenal> PenalCausaPenalProceso(@PathVariable("id") Integer id) {
		List<ProcesoPenal> lista = new ArrayList<>();

		ProcesoPenal pp = procesoPenalService.findByIdCausaPenal(id);
		if(pp != null) {
			lista.add(pp);
			return  lista;
		}else {
			return null;
		}
		
	}
	
	
	
	@PostMapping("/addProcesoPenal")
	@ResponseBody
	public List<ProcesoPenal> addProcesoPenal(@RequestBody ProcesoPenalVO procesoPenal) {
		List<ProcesoPenal> lista = new ArrayList<>();

		CausaPenal causaPenal = penalCausaPenalService.findById(procesoPenal.getIdCausaPenal());

		ProcesoPenal pp =  procesoPenalService.findByIdCausaPenal(causaPenal.getId());
		
		if(pp == null) {
			pp = new ProcesoPenal();
			pp.setIdCausaPenal(causaPenal.getId());
			pp.setProcesado(causaPenal.getAcusado());
			pp.setTipoDefensor(false);
			pp.setFechaDetencion(procesoPenal.getFechaDetencion());
			pp.setFechaUltimaActuacion(procesoPenal.getFechaUltimaActuacion());
			pp.setObservaciones(procesoPenal.getObservaciones());
			procesoPenalService.addProcesoPenal(pp);
		}else {
		}

		if(pp != null) {	
			switch(procesoPenal.getTipoProceso()) {
				case 1:
					PenalAmparo pa  = penalAmparoService.findByProcesoPenalId(pp.getId());
					if(pa == null) {
						pa = new PenalAmparo();
						pa.setProcesoPenal(pp);
						pa.setEstatus(true);
						pa.setFechaAmpara(procesoPenal.getFechaAmpara());
						pa.setFechaNiega(procesoPenal.getFechaNiega());
						pa.setFechaProteje(procesoPenal.getFechaProtege());
						pa.setFechaSobree(procesoPenal.getFechaProtege());
						pa.setNumeroAmparo((long) 4);
						pa.setIdAutoridadFederal(1);
						pa.setFechaNotDemanda(new Date());
						penalAmparoService.addPenalAmparo(pa);
					}
				break;
				case 2:
					PenalDetenido pd  = penalDetenidoService.findByProcesoPenalId(pp.getId());
					if(pd == null) {
						pd = new PenalDetenido();
						pd.setFechaDetencion(procesoPenal.getFechaDetencion());
						pd.setProcesoPenal(pp);
						penalDetenidoService.addPenalDetenido(pd);
					}
				break;
				
				case 3:
					ProcesoPenalBajoFianza bf  = penalBajoFianzaService.findByProcesoPenalId(pp.getId());
					if(bf == null) {
						bf = new ProcesoPenalBajoFianza();
						bf.setObservaciones(procesoPenal.getObservacionesBajoFianza());
						bf.setProcesoPenal(pp);
						penalBajoFianzaService.addProcesoPenalBajoFianza(bf);
					}
				break;
				
				case 4:
					ProcesoPenalFormalPrision fp  = penalFormalPrisionService.findByProcesoPenalId(pp.getId());
					if(fp == null) {
					fp = new ProcesoPenalFormalPrision();
					fp.setProcesoPenal(pp);
					fp.setFechaAcuerdo(procesoPenal.getFechaAcuerdo());
					fp.setFechaApelacion(procesoPenal.getFechaApelacion());
					fp.setTipoSentencia(sentenciaService.findById(procesoPenal.getSentencia()));
					penalFormalPrisionService.addProcesoPenalFormalPrision(fp);
					}
				break;
				}
		}
		lista.add(pp);
		return lista;
	}
	

	
	
	@GetMapping("/autoridades/")
	@ResponseBody
	public List<Autoridad> autoridades() {
		return autoridadService.findByEstatus(true);
	}
	
	

	@GetMapping("/procesoPenalAmparo/{id}")
	@ResponseBody
	public List<PenalAmparo> ProcesoPenalAmparo(@PathVariable("id") Integer id) {
		List<PenalAmparo> lista = new ArrayList<>();
		PenalAmparo pp = penalAmparoService.findByProcesoPenalId(id);

		lista.add(pp);
		return  lista;
	}
	
	@GetMapping("/procesoPenalDetenido/{id}")
	@ResponseBody
	public List<PenalDetenido> ProcesoPenalDetenido(@PathVariable("id") Integer id) {
		List<PenalDetenido> lista = new ArrayList<>();
		PenalDetenido pp = penalDetenidoService.findByProcesoPenalId(id);

		lista.add(pp);
		return  lista;
	}
	
	
	@GetMapping("/procesoPenalBajoFianza/{id}")
	@ResponseBody
	public List<ProcesoPenalBajoFianza> procesoPenalBajoFianza(@PathVariable("id") Integer id) {
		List<ProcesoPenalBajoFianza> lista = new ArrayList<>();

		ProcesoPenalBajoFianza pp = penalBajoFianzaService.findByProcesoPenalId(id);

		lista.add(pp);
		return  lista;
	}
	
	
	
	@GetMapping("/procesoPenalFormalPrision/{id}")
	@ResponseBody
	public List<ProcesoPenalFormalPrision> procesoPenalFormalPrision(@PathVariable("id") Integer id) {
		List<ProcesoPenalFormalPrision> lista = new ArrayList<>();

		ProcesoPenalFormalPrision pp = penalFormalPrisionService.findByProcesoPenalId(id);

		lista.add(pp);
		return  lista;
	}
	
	@GetMapping("/sentencias/")
	@ResponseBody
	public List<TipoSentencia> allSentencias() {
		return sentenciaService.listAll();
	}
}
