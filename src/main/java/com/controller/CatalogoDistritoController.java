package com.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.entity.Distrito;
import com.entity.Municipio;
import com.service.DistritoService;
import com.service.MunicipioService;

@Controller
@RequestMapping("/catalogoDistrito")
public class CatalogoDistritoController implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	@Qualifier("distritoServiceImpl")
	private DistritoService distritoService;
	
	@Autowired
	@Qualifier("municipioServiceImpl")
	private MunicipioService municipioService;
		
	@GetMapping("/inicio")
	public ModelAndView listAllCursos() {
		ModelAndView mv =  new ModelAndView("catalogoDistrito");
		return mv;
	}
	
	
	@PostMapping("/add")
	@ResponseBody
	public List<Distrito> add(@RequestBody Distrito distrito) {
		distrito.setEstatus(true);
		distrito.setMunicipio(municipioService.findById(  Integer.parseInt(distrito.getMunicipioV())));
		distritoService.addDistrito(distrito);
		return distritoService.listAllDistritos();
	}
	
	@GetMapping("/distrito/{id}")
	@ResponseBody
	public List<Distrito> Distrito(@PathVariable("id") Integer id) {
		List<Distrito> lista = new ArrayList<>();
		lista.add(distritoService.findById(id));
		return  lista;
	}
	
	
	
	@GetMapping("/distrito/del/{id}")
	@ResponseBody
	public void del(@PathVariable("id") Integer id) {
		Distrito Distrito = distritoService.findById(id);
		Distrito.setEstatus(false);
		distritoService.updatDistrito(Distrito);
	}
	
	
	@GetMapping("/all/")
	@ResponseBody
	public List<Distrito> all() {
		return distritoService.findByEstatus(true);
	}
	
	@GetMapping("/municipios/")
	@ResponseBody
	public List<Municipio> allMunicipios() {
		return municipioService.findByEstatus(true);
	}
	
}
