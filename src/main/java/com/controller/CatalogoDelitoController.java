package com.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.entity.Delito;
import com.service.DelitoService;

@Controller
@RequestMapping("/catalogoDelito")
public class CatalogoDelitoController {
	
	
	@Autowired
	@Qualifier("delitoServiceImpl")
	private DelitoService delitoService;
		
	@GetMapping("/inicio")
	public ModelAndView listAllDelitos() {
		ModelAndView mv =  new ModelAndView("catalogoDelito");
		return mv;
	}
	
	
	@PostMapping("/add")
	@ResponseBody
	public List<Delito> add(@RequestBody Delito delito) {
		delito.setEstatus(true);
		delitoService.addDelito(delito);
		return delitoService.listAllDelitos();
	}
	
	@GetMapping("/delito/{id}")
	@ResponseBody
	public List<Delito> Delito(@PathVariable("id") Integer id) {
		List<Delito> lista = new ArrayList<>();
		lista.add(delitoService.findById(id));
		return  lista;
	}
	
	
	
	@GetMapping("/delito/del/{id}")
	@ResponseBody
	public void del(@PathVariable("id") Integer id) {
		Delito Delito = delitoService.findById(id);
		Delito.setEstatus(false);
		delitoService.updatDelito(Delito);
	}
	
	
	@GetMapping("/all/")
	@ResponseBody
	public List<Delito> all() {
		return delitoService.findByEstatus(true);
	}
	
}
