package com.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.entity.Distrito;
import com.entity.Juzgado;
import com.entity.Municipio;
import com.entity.Ramo;
import com.service.DistritoService;
import com.service.JuzgadoService;
import com.service.MunicipioService;
import com.service.RamoService;

@Controller
@RequestMapping("/catalogoJuzgado")
public class CatalogoJuzgadoController {
	
	
	@Autowired
	@Qualifier("juzgadoServiceImpl")
	private JuzgadoService juzgadoService;
	
	@Autowired
	@Qualifier("ramoServiceImpl") 
	private RamoService ramoService;
	
	@Autowired
	@Qualifier("municipioServiceImpl")
	private MunicipioService municipioService;
	
	@Autowired
	@Qualifier("distritoServiceImpl")
	private DistritoService distritoService;
		
	@GetMapping("/inicio")
	public ModelAndView listAllCursos() {
		ModelAndView mv =  new ModelAndView("catalogoJuzgado");
		return mv;
	}
	
	
	@PostMapping("/add")
	@ResponseBody
	public List<Juzgado> add(@RequestBody Juzgado juzgado) {
		juzgado.setEstatus(true);
		Distrito  distrito = distritoService.findById(juzgado.getIdDistritoV());
		Ramo ramo = ramoService.findById(juzgado.getIdRamoV());
		Municipio  municipio = municipioService.findById(juzgado.getIdMunicipioV());
		
		juzgado.setRamo(ramo);
		juzgado.setMunicipio(municipio);
		juzgado.setDistrito(distrito);
		
		if(juzgado.getId() != null) {
			juzgadoService.addJuzgado(juzgado);
		}else {
			juzgadoService.updatJuzgado(juzgado);
		}
		return juzgadoService.listAllJuzgados();
	}
	
	@GetMapping("/juzgado/{id}")
	@ResponseBody
	public List<Juzgado> Juzgado(@PathVariable("id") Integer id) {
		List<Juzgado> lista = new ArrayList<>();
		lista.add(juzgadoService.findById(id));
		return  lista;
	}
	
	
	
	@GetMapping("/juzgado/del/{id}")
	@ResponseBody
	public void del(@PathVariable("id") Integer id) {
		Juzgado Juzgado = juzgadoService.findById(id);
		Juzgado.setEstatus(false);
		juzgadoService.updatJuzgado(Juzgado);
	}
	
	
	@GetMapping("/all/")
	@ResponseBody
	public List<Juzgado> all() {
		return juzgadoService.findByEstatus(true);
	}
	
	
	@GetMapping("/ramos/")
	@ResponseBody
	public List<Ramo> allRamos() {
		return ramoService.findByEstatus(true);
	}
	
	
	@GetMapping("/municipios/")
	@ResponseBody
	public List<Municipio> allMunicipios() {
		return municipioService.findByEstatus(true);
	}
	
	
	@GetMapping("/distritos/")
	@ResponseBody
	public List<Distrito> allDistritos() {
		return distritoService.findByEstatus(true);
	}
	
	
	@GetMapping("/nombreJuzgado/{id}/{idDistrito}")
	@ResponseBody
	public List<String> distritoFindById(@PathVariable("id") Integer id, @PathVariable("idDistrito") Integer idDistrito) {
		Ramo ramo = ramoService.findById(id);
		Distrito  distrito = distritoService.findById(idDistrito);
		List<String> lista = new ArrayList<>();
		lista.add(ramo.getNombre() +" "+distrito.getNombre());
		return lista;
	}
}
