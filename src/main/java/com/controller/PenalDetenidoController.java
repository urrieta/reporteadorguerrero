package com.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.entity.PenalDetenido;
import com.service.PenalDetenidoService;

@Controller
@RequestMapping("/penalDetenido")
public class PenalDetenidoController {
	
	
	@Autowired
	@Qualifier("penalDetenidoServiceImpl")
	private PenalDetenidoService penalDetenidoService;
		
	@GetMapping("/inicio")
	public ModelAndView listAllCursos() {
		ModelAndView mv =  new ModelAndView("penalDetenido");
		return mv;
	}
	
	@PostMapping("/add")
	@ResponseBody
	public List<PenalDetenido> add(@RequestBody PenalDetenido penalDetenido) {
	//	penalDetenido.setEstatus(true);
	//	penalDetenido.setTipoDefensor(true);
		penalDetenidoService.addPenalDetenido(penalDetenido);
		return penalDetenidoService.listAllPenalDetenidos();
	}
	
	@GetMapping("/penalDetenido/{id}")
	@ResponseBody
	public List<PenalDetenido> PenalDetenido(@PathVariable("id") Integer id) {
		List<PenalDetenido> lista = new ArrayList<>();
		lista.add(penalDetenidoService.findById(id));
		return  lista;
	}
	
	
	@GetMapping("/penalDetenido/del/{id}")
	@ResponseBody
	public void del(@PathVariable("id") Integer id) {
		PenalDetenido penalDetenido = penalDetenidoService.findById(id);
		penalDetenidoService.updatPenalDetenido(penalDetenido);
	}
	
	
	@GetMapping("/all/")
	@ResponseBody
	public List<PenalDetenido> all() {
		return penalDetenidoService.findByEstatus(true);
	}
	
}
