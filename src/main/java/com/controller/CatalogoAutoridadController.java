package com.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.entity.Autoridad;
import com.service.AutoridadService;

@Controller
@RequestMapping("/catalogoAutoridad")
public class CatalogoAutoridadController {
	
	
	@Autowired
	@Qualifier("autoridadServiceImpl")
	private AutoridadService autoridadService;
		
	@GetMapping("/inicio")
	public ModelAndView listAllCursos() {
		ModelAndView mv =  new ModelAndView("catalogoAutoridad");
		return mv;
	}
	
	
	@PostMapping("/add")
	@ResponseBody
	public List<Autoridad> add(@RequestBody Autoridad autoridad) {
		autoridad.setEstatus(true);
		autoridadService.addAutoridad(autoridad);
		return autoridadService.listAllAutoridads();
	}
	
	@GetMapping("/autoridad/{id}")
	@ResponseBody
	public List<Autoridad> Autoridad(@PathVariable("id") Integer id) {
		List<Autoridad> lista = new ArrayList<>();
		lista.add(autoridadService.findById(id));
		return  lista;
	}
	
	
	
	@GetMapping("/autoridad/del/{id}")
	@ResponseBody
	public void del(@PathVariable("id") Integer id) {
		Autoridad autoridad = autoridadService.findById(id);
		autoridad.setEstatus(false);
		autoridadService.updatAutoridad(autoridad);
	}
	
	
	@GetMapping("/all/")
	@ResponseBody
	public List<Autoridad> all() {
		return autoridadService.findByEstatus(true);
	}
	
}
