package com.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.service.PenalCausaPenalService;
import com.tools.ExportUtils;

@RestController
@RequestMapping("/download")
public class FileController {

	private static final String EXTERNAL_FILE_PATH = "C:/reportes_guerrero/jasper/causaPenal1.pdf";
	
	@Autowired
	@Qualifier("penalCausaPenalServiceImpl")
	private PenalCausaPenalService penalCausaPenalService;

	@RequestMapping("/file/")
	public void downloadPDFResource(HttpServletRequest request, HttpServletResponse response
			) throws IOException {
		generarReporte();
		File file = new File(EXTERNAL_FILE_PATH );
		if (file.exists()) {

			String mimeType = URLConnection.guessContentTypeFromName(file.getName());
			if (mimeType == null) {
				mimeType = "application/octet-stream";
			}
			response.setContentType(mimeType);
			response.setHeader("Content-Disposition", String.format("inline; filename=\"" + file.getName() + "\""));
			response.setContentLength((int) file.length());
			InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
			FileCopyUtils.copy(inputStream, response.getOutputStream());
		}
	}
	
	public void generarReporte() {
		ExportUtils export =  new ExportUtils();
		export.createReportSMP("C:/reportes_guerrero/jasper/causaPenal1.pdf",   penalCausaPenalService.reporteAdminPenalUno(),"C:/reportes_guerrero/jasper/causaPenal" );
	}
}
