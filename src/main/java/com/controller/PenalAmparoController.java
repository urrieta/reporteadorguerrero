package com.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.entity.Agraviado;
import com.entity.Autoridad;
import com.entity.Delito;
import com.entity.PenalAmparo;
import com.entity.CausaPenal;
import com.service.AcusadoService;
import com.service.AgraviadoService;
import com.service.AutoridadService;
import com.service.DelitoService;
import com.service.JuzgadoService;
import com.service.PenalAmparoService;
//import com.tools.ExportUtils;
import com.service.PenalCausaPenalService;

@Controller
@RequestMapping("/penalAmparo")
public class PenalAmparoController {
	
	
	@Autowired
	@Qualifier("penalAmparoServiceImpl")
	private PenalAmparoService penalAmparoService;
	
	@Autowired
	@Qualifier("delitoServiceImpl")
	private DelitoService delitoService;
	
	@Autowired
	@Qualifier("juzgadoServiceImpl")
	private JuzgadoService juzgadoService;
	
	@Autowired
	@Qualifier("autoridadServiceImpl")
	private AutoridadService autoridadService;
		
	@Autowired
	@Qualifier("agraviadoServiceImpl")
	private AgraviadoService agraviadoService;
	
	
	@Autowired
	@Qualifier("acusadoServiceImpl")
	private AcusadoService acusadoService;
	
	@Autowired
	@Qualifier("penalCausaPenalServiceImpl")
	private PenalCausaPenalService penalCausaPenalService;
	

	
		
	private CausaPenal causaPenal;
	@GetMapping("/inicio")
	public ModelAndView listAllCursos() {
		ModelAndView mv =  new ModelAndView("penalAmparo");
		return mv;
	}
	
	@GetMapping("/allPenalCausaPenales/")
	@ResponseBody
	public List<CausaPenal> allPenalCausaPenales() {
		System.out.println(" all PenalCausaPenals");
	//	return penalCausaPenalService.findByEstatus(true);
		return null;
	}
	
	
	@GetMapping("/penalCausaPenal/{id}")
	@ResponseBody
	public List<CausaPenal> PenalCausaPenal(@PathVariable("id") Integer id) {
		List<CausaPenal> lista = new ArrayList<>();
		causaPenal = penalCausaPenalService.findById(id);
		List<Agraviado> agraviados = agraviadoService.findByIdPenalCausaPenal(id);
		causaPenal.setAgraviado(agraviados.get(0));
		lista.add(causaPenal);
		return  lista;
	}
	
	
	
	@PostMapping("/add")
	@ResponseBody
	public List<PenalAmparo> add(@RequestBody PenalAmparo penalAmparo) {
		penalAmparo.setEstatus(true);
		if(penalAmparo.getId() != null) {
			penalAmparoService.updatPenalAmparo(penalAmparo);
		}else {
			penalAmparoService.addPenalAmparo(penalAmparo);
		}
		return penalAmparoService.listAllPenalAmparos();
	}
	
	@GetMapping("/penalAmparo/{id}")
	@ResponseBody
	public List<PenalAmparo> PenalAmparo(@PathVariable("id") Integer id) {
		List<PenalAmparo> lista = new ArrayList<>();
		lista.add(penalAmparoService.findById(id));
		return  lista;
	}
	
	
	@GetMapping("/penalAmparo/del/{id}")
	@ResponseBody
	public void del(@PathVariable("id") Integer id) {
		PenalAmparo penalAmparo = penalAmparoService.findById(id);
		penalAmparo.setEstatus(false);
		penalAmparoService.updatPenalAmparo(penalAmparo);
	}
	
	
	@GetMapping("/allPenalAmparoes/")
	@ResponseBody
	public List<PenalAmparo> all() {
		List<PenalAmparo> lista = penalAmparoService.findByEstatus(true);
		return lista;
	}
	
	
	@GetMapping("/delitos/")
	@ResponseBody
	public List<Delito> allDelitos() {
		return delitoService.listAllDelitos();
	}
	
	
	@GetMapping("/autoridades/")
	@ResponseBody
	public List<Autoridad> allDialectos() {
		return autoridadService.listAllAutoridads();
	}
	

	public static Date ParseFecha(String fecha){
        SimpleDateFormat formato = new SimpleDateFormat("MM/dd/yyyy");
        Date fechaDate = null;
        try {
        	if(fecha != null && !fecha.equals("")) {
        		fechaDate = formato.parse(fecha);
        	}
        }catch (ParseException ex){
        }
        return fechaDate;
    }
	
	public static Date ParseFechaAct(String fecha){
        SimpleDateFormat formato = new SimpleDateFormat("MM-dd-yyyy");
        Date fechaDate = null;
        try {
        	if(fecha != null && !fecha.equals("")) {
        		fechaDate = formato.parse(fecha);
        	}
        }catch (ParseException ex){
        }
        return fechaDate;
    }
	
}
