package com.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.entity.Municipio;
import com.service.MunicipioService;

@Controller
@RequestMapping("/catalogoMunicipio")
public class CatalogoMunicipioController {
	
	
	@Autowired
	@Qualifier("municipioServiceImpl")
	private MunicipioService municipioService;
		
	@GetMapping("/inicio")
	public ModelAndView listAllCursos() {
		ModelAndView mv =  new ModelAndView("catalogoMunicipio");
		return mv;
	}
	
	@PostMapping("/add")
	@ResponseBody
	public List<Municipio> add(@RequestBody Municipio municipio) {
		municipio.setEstatus(true);
		municipioService.addMunicipio(municipio);
		return municipioService.listAllMunicipios();
	}
	
	@GetMapping("/municipio/{id}")
	@ResponseBody
	public List<Municipio> municipio(@PathVariable("id") Integer id) {
		List<Municipio> lista = new ArrayList<>();
		lista.add(municipioService.findById(id));
		return  lista;
	}
	
	
	@GetMapping("/municipio/del/{id}")
	@ResponseBody
	public void del(@PathVariable("id") Integer id) {
		Municipio municipio = municipioService.findById(id);
		municipio.setEstatus(false);
		municipioService.updatMunicipio(municipio);
	}
	
	
	@GetMapping("/allMunicipios/")
	@ResponseBody
	public List<Municipio> all() {
		return municipioService.findByEstatus(true);
	}
	
}
