package com.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.entity.MateriaCivil;
import com.service.MateriaCivilService;

@Controller
@RequestMapping("/materiaCivil")
public class MateriaCivilController {
	
	@Autowired
	@Qualifier("materiaCivilServiceImpl")
	private MateriaCivilService materiaCivilService;
	
	@GetMapping("/all/")
	@ResponseBody
	public List<MateriaCivil> all() {
		return materiaCivilService.findByEstatus(true);
	}
	
	@GetMapping("/radicadas")
	public ModelAndView listMateriaCivil() {
		ModelAndView mv = new ModelAndView("materiaCivil");
		return mv;
	}
	
	@PostMapping("/create")
	@ResponseBody
	public List<MateriaCivil> create(@RequestBody MateriaCivil materiaCivil) {
		materiaCivil.setEstatus(true);
		if (materiaCivil.getId() != null) {
			materiaCivilService.updateMateriaCivil(materiaCivil);
		} else {
			materiaCivilService.addMateriaCivil(materiaCivil);
		}
		
		return materiaCivilService.listAllMateriaCivil();
	}
	
	@GetMapping("/materiaCivil/{id}")
	@ResponseBody
	public List<MateriaCivil> materiaCivil(@PathVariable("id") Integer id) {
		List<MateriaCivil> lista = new ArrayList<MateriaCivil>();
		
		MateriaCivil mc = materiaCivilService.findById(id);
		
		lista.add(mc);
		
		return lista;
	}
	
	
	
	@GetMapping("/delete/{id}")
	@ResponseBody
	public void delete(@PathVariable("id") Integer id) {
		MateriaCivil materiaCivil = materiaCivilService.findById(id);
		materiaCivil.setEstatus(false);
		materiaCivilService.updateMateriaCivil(materiaCivil);
	}
}
