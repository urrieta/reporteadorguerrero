package com.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.entity.PenalCausasTramite;
import com.service.PenalCausasTramiteService;

@Controller
@RequestMapping("/penalCausasTramite")
public class PenalCausasTramiteController {
	
	
	@Autowired
	@Qualifier("penalCausasTramiteServiceImpl")
	private PenalCausasTramiteService penalCausasTramiteService;
		
	@GetMapping("/inicio")
	public ModelAndView listAllCursos() {
		ModelAndView mv =  new ModelAndView("penalCausasTramite");
		return mv;
	}
	
	@PostMapping("/add")
	@ResponseBody
	public List<PenalCausasTramite> add(@RequestBody PenalCausasTramite penalCausasTramite) {
		penalCausasTramite.setEstatus(true);
		penalCausasTramiteService.addPenalCausasTramite(penalCausasTramite);
		return penalCausasTramiteService.listAllPenalCausasTramites();
	}
	
	@GetMapping("/penalCausasTramite/{id}")
	@ResponseBody
	public List<PenalCausasTramite> PenalCausasTramite(@PathVariable("id") Integer id) {
		List<PenalCausasTramite> lista = new ArrayList<>();
		lista.add(penalCausasTramiteService.findById(id));
		return  lista;
	}
	
	
	@GetMapping("/penalCausasTramite/del/{id}")
	@ResponseBody
	public void del(@PathVariable("id") Integer id) {
		PenalCausasTramite penalCausasTramite = penalCausasTramiteService.findById(id);
		penalCausasTramite.setEstatus(false);
		penalCausasTramiteService.updatPenalCausasTramite(penalCausasTramite);
	}
	
	
	@GetMapping("/allPenalCausasTramites/")
	@ResponseBody
	public List<PenalCausasTramite> all() {
		return penalCausasTramiteService.findByEstatus(true);
	}
	
}
