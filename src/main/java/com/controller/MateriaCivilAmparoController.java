package com.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.entity.MateriaCivilAmparo;
import com.service.MateriaCivilAmparoService;

@Controller
@RequestMapping("/materiaCivilAmparo")
public class MateriaCivilAmparoController {

	@Autowired
	@Qualifier("materiaCivilAmparoServiceImpl")
	private MateriaCivilAmparoService materiaCivilAmparoService;

	@GetMapping("/all/")
	@ResponseBody
	public List<MateriaCivilAmparo> all() {
		return materiaCivilAmparoService.findByEstatus(true);
	}

	@GetMapping("/Amparos")
	public ModelAndView listMateriaCivilAmparo() {
		ModelAndView mv = new ModelAndView("materiaCivilAmparo");
		return mv;
	}

	@PostMapping("/create")
	@ResponseBody
	public List<MateriaCivilAmparo> create(@RequestBody MateriaCivilAmparo materiaCivilAmparo) {
		materiaCivilAmparo.setEstatus(true);
		if (materiaCivilAmparo.getId() != null) {
			materiaCivilAmparoService.updateMateriaCivilAmparo(materiaCivilAmparo);
		} else {
			materiaCivilAmparoService.addMateriaCivilAmparo(materiaCivilAmparo);
		}
		return materiaCivilAmparoService.listAllMateriaCivilAmparos();
	}
	
	@GetMapping("/materiaCivilAmparo/{id}")
	@ResponseBody
	public List<MateriaCivilAmparo> materiaCivil(@PathVariable("id") Integer id) {
		List<MateriaCivilAmparo> lista = new ArrayList<MateriaCivilAmparo>();
		
		MateriaCivilAmparo mc = materiaCivilAmparoService.findById(id);
		
		lista.add(mc);
		
		return lista;
	}
	
	
	
	@GetMapping("/delete/{id}")
	@ResponseBody
	public void delete(@PathVariable("id") Integer id) {
		MateriaCivilAmparo materiaCivilAmparo = materiaCivilAmparoService.findById(id);
		materiaCivilAmparo.setEstatus(false);
		materiaCivilAmparoService.updateMateriaCivilAmparo(materiaCivilAmparo);
	}
	
	
}
