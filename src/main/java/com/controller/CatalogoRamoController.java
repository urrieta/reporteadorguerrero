package com.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.entity.Ramo;
import com.service.RamoService;

@Controller
@RequestMapping("/catalogoRamo")
public class CatalogoRamoController {
	
	
	@Autowired
	@Qualifier("ramoServiceImpl")
	private RamoService ramoService;
		
	@GetMapping("/inicio")
	public ModelAndView listAllCursos() {
		ModelAndView mv =  new ModelAndView("catalogoRamo");
		return mv;
	}
	
	
	@PostMapping("/add")
	@ResponseBody
	public List<Ramo> add(@RequestBody Ramo Ramo) {
		Ramo.setEstatus(true);
		ramoService.addRamo(Ramo);
		return ramoService.listAllRamos();
	}
	
	@GetMapping("/ramo/{id}")
	@ResponseBody
	public List<Ramo> Ramo(@PathVariable("id") Integer id) {
		List<Ramo> lista = new ArrayList<>();
		lista.add(ramoService.findById(id));
		return  lista;
	}
	
	
	
	@GetMapping("/ramo/del/{id}")
	@ResponseBody
	public void del(@PathVariable("id") Integer id) {
		Ramo ramo = ramoService.findById(id);
		ramo.setEstatus(false);
		ramoService.updatRamo(ramo);
	}
	
	
	@GetMapping("/all/")
	@ResponseBody
	public List<Ramo> all() {
		return ramoService.findByEstatus(true);
	}
	
}
