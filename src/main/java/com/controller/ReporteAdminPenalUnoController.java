package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.dto.ReporteAdminPenalUnoDTO;
import com.service.PenalCausaPenalService;

@Controller
public class ReporteAdminPenalUnoController {
	
	@Autowired
	@Qualifier("penalCausaPenalServiceImpl")
	private PenalCausaPenalService penalCausaPenalService;
	
	@GetMapping("/Uno")
	public ModelAndView reporteUno() {
		ModelAndView mv =  new ModelAndView("PenalUno");
		return mv;
	}
	
	@GetMapping("/registrosPenalUno/")
	@ResponseBody
	public List<ReporteAdminPenalUnoDTO> obtenerReporteUno() {
		return penalCausaPenalService.reporteAdminPenalUno();
	}
	
}
