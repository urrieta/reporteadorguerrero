package com.controller;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.security.core.context.SecurityContextHolder;

import com.entity.User;
import com.service.UserService;


@Controller
public class InicioController {
	
	public Authentication auth;
	
	@Autowired
	UserService usuarioService;
	
	@PostConstruct
	public void init() {
	   

	}
	
	
	@GetMapping("/inicio")
	public ModelAndView inicio() {

			auth = SecurityContextHolder.getContext().getAuthentication();
		    UserDetails userDetail = (UserDetails) auth.getPrincipal();
		    User usuario = this.usuarioService.findByUsername(userDetail.getUsername());
		    ModelAndView mv;

		    if(usuario.getRoles().iterator().next().getName().equals("ROLE_ADMIN")){
		    	 mv = new ModelAndView("inicioAdministrador");
		    }else{
		    	mv = new ModelAndView("inicioUser");
		    }
		    mv.addObject("usuario",usuario);
		    
		    return mv;
	}
	
		
}
