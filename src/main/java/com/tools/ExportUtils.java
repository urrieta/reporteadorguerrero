package com.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.dto.ReporteAdminPenalUnoDTO;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;



public class ExportUtils {

	private static final Logger log = Logger.getLogger(ExportUtils.class.getName());

	public void createReportSMP( String name, List<ReporteAdminPenalUnoDTO> registros, String nameJasper) {
		try {
			JRBeanCollectionDataSource itemJRBean = new JRBeanCollectionDataSource(registros);
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("registros",itemJRBean );
		    String path = nameJasper+".jrxml";
			InputStream input = new FileInputStream(new File (path));
			JasperDesign jasperDesign = JRXmlLoader.load(input);
	        JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
	
			path =name;
			JasperExportManager.exportReportToPdfFile(jasperPrint, path);
			
        } catch (JRException ex) {
            log.severe("Erro1r: "+ex.getMessage());
        } catch (IOException e) {
			e.printStackTrace();
            log.severe("Erro2r: "+e.getMessage());
		}
	}
	
	
	
}